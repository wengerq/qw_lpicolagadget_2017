# ##### BEGIN GPL LICENSE BLOCK #####
#
#    ps_theory.py - Draw PS from PyCosmo prediction for different z
#    Copyright (C) 2017  Quentin Wenger
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####


import os

import matplotlib.pyplot as plt
import numpy


z_values = [
    50.000,
    9.000,
    1.997,
    1.488,
    0.511,
    0.211,
    0.103,
    0.000]



out_path = "./ps_theory.png"



def getPowerSpectrum_Ref(z_value):
    base_path = "/some/path/downloads/L-PICOLA/"\
        "Quentin/theory_w_gadget_params"
    fname = "pk_nonlin_z%s.npy" % ("%.3f" % z_value).replace(".", "p")

    npy_file = os.path.join(base_path, fname)

    # XXXX: temp
    data = numpy.load(npy_file)
    return (data[:, 0], data[:, 1])




plt.fill_between([1e-1, 1e1], 1e5, 1e-5, color="yellow")

for z_value in z_values:
    h_value = 0.7
    ps_ref = getPowerSpectrum_Ref(z_value)
    plt.loglog(
        ps_ref[0]/h_value, ps_ref[1]*h_value**3, label="$z = %.3f$" % z_value)

plt.title(r"PS Theory")
plt.xlabel(r"$k$ ($(a \cdot \mathrm{Mpc} \cdot h^{-1})^{-1}$)")
plt.ylabel(r"$P_k$ ($(a \cdot \mathrm{Mpc} \cdot h^{-1})^3$)")
plt.legend(loc="lower left")
#plt.show()
plt.savefig(out_path)


