# ##### BEGIN GPL LICENSE BLOCK #####
#
#    power_spectra_all_nmesh512_1024.py - Draw PS of snapshots for different
#    L-PICOLA Nmesh parameter values
#    Copyright (C) 2017  Quentin Wenger
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####


import os
import sys
import re
import math
import itertools
import collections

import pynbody
import matplotlib.pyplot as plt
import numpy


sys.path.append("../..")
from qw_analyse_gadget.analysis import power_spectrum
from qw_analyse_gadget.utils import smb_downloader
from qw_analyse_gadget.utils import files_manager



Z_VALUES = [
    0.000,
    #0.103,
    #0.211,
    #0.511,
    #1.488,
    #1.997,
    ]

BOXES = collections.OrderedDict([
    #("50Mpc", [256]),
    ("100Mpc", [128]),#256, 512]),#####
    ("1Gpc", [256]),
    #("4Gpc", [256]),
    ])

bins_amounts = lambda b: {"100Mpc": 100, "1Gpc": 100}[b]


CACHE_PATH = os.path.abspath("./downloads")
OUTPUTS_PATH = os.path.abspath("./outputs")

BASE_DATA_DIR = "/refreg/data"



def cacheSMBFile(dir_path, filename):
    local_path = os.path.join(CACHE_PATH, dir_path, filename)
    print "Looking at SMB file..."
    print "     dir path: " + dir_path
    print "    file name: " + filename
    if os.path.exists(local_path):
        print "    using cached file in folder %s." % CACHE_PATH
    else:
        with files_manager.LocalFile(local_path) as (fd, _):
            print "    downloading to folder %s..." % CACHE_PATH
            smb_downloader.retrieveFile(
                os.path.join(BASE_DATA_DIR, dir_path, filename), fd)
            fd.close()
            print "    done.\n"
    return CACHE_PATH, os.path.join(dir_path, filename)


def listSMBDir_Dirs(dir_path):
    return smb_downloader.getDirsList(os.path.join(BASE_DATA_DIR, dir_path))

def listSMBDir_Files(dir_path):
    return smb_downloader.getFilesList(os.path.join(BASE_DATA_DIR, dir_path))



def getDataFiles_Gadget(z_value, size, bins):
    base_path = "cpod"

    if size == "100Mpc":
        sim_pattern = re.compile(
            r"^Sim_3D_[0-9]{8}_[0-9]{6}_%s$" % bins)
    else:
        sim_pattern = re.compile(
            r"^Sim_3D_[0-9]{8}_[0-9]{6}_%s_%s$" % (bins, size))

    # just pick up the first corresponding one...
    folder = os.path.join(base_path, [
        d for d in listSMBDir_Dirs(base_path)
        if re.match(sim_pattern, d)][0])

    with open(
        os.path.join(*cacheSMBFile(folder, "outfile_a.dat")), "r") as a_data:
        a_values = [float(line.strip("\n")) for line in a_data.readlines()]

    a_value = 1.0/(1.0 + z_value)
    nearest_a = sorted(a_values, key=lambda a: abs(a - a_value))[0]
    nearest_z = 1.0/nearest_a - 1.0

    # XXX: what about snapshot_000???
    return [cacheSMBFile(
        os.path.join(folder, "3D_boxes"),
        "snapshot_%03i" % (a_values.index(nearest_a) + 1))], nearest_z




def getDataFiles_LPicola(z_value, size, bins):
    base_path = "L-PICOLA/Quentin/l-picola_w_gadget_params/Box_%s_o_h" % size

    pattern = re.compile(r"^out_z%s\.[0-9]+$" % (
        "%.3f" % z_value).replace(".", "p"))

    # hack
    if bins == 128:
        base_path = os.path.join(base_path, "Npart_%s" % bins)
    # else we assume the bins to correspond!
    
    files = listSMBDir_Files(base_path)

    return [
        cacheSMBFile(base_path, f) for f in files
        if re.match(pattern, f)], z_value



### TODO: share code
def getDataFiles_LPicola_nmesh512(z_value, size, bins):
    base_path = "L-PICOLA/Quentin/l-picola_w_gadget_params/Nmesh_512/Box_%s_o_h" % size

    pattern = re.compile(r"^out_z%s\.[0-9]+$" % (
        "%.3f" % z_value).replace(".", "p"))

    # hack
    if bins == 128:
        base_path = os.path.join(base_path, "Npart_%s" % bins)
    # else we assume the bins to correspond!
    
    files = listSMBDir_Files(base_path)

    return [
        cacheSMBFile(base_path, f) for f in files
        if re.match(pattern, f)], z_value


### TODO: share code
def getDataFiles_LPicola_nmesh1024(z_value, size, bins):
    base_path = "L-PICOLA/Quentin/l-picola_w_gadget_params/Nmesh_1024/Box_%s_o_h" % size

    pattern = re.compile(r"^out_z%s\.[0-9]+$" % (
        "%.3f" % z_value).replace(".", "p"))

    # hack
    if bins == 128:
        base_path = os.path.join(base_path, "Npart_%s" % bins)
    # else we assume the bins to correspond!
    
    files = listSMBDir_Files(base_path)

    return [
        cacheSMBFile(base_path, f) for f in files
        if re.match(pattern, f)], z_value




def getFileNameBase(files):
    # we can assume all files to be in the same dir
    dir_path = os.path.dirname(files[0])
    # inspired by http://stackoverflow.com/questions/18715688/find-common-substring-between-two-strings
    def _iter():
        for chars in zip(*[os.path.basename(f) for f in files]):
            if len(set(chars)) == 1:
                yield chars[0]
            else:
                return


    common_name = ''.join(_iter())
    if not common_name:
        # XXX: anything better???
        common_name = "dummy"

    return os.path.join(dir_path, common_name)




def getPowerSpectrum_Sim(fct, z_value, size, bins):

    files, nearest_z = fct(z_value, size, bins)

    if not files:
        return ([], []), z_value    

    power_base = os.path.join(
        OUTPUTS_PATH, getFileNameBase([f[1] for f in files]) + "_ps")
    power_k = power_base + "_k"
    power_p = power_base + "_p"

    if os.path.exists(power_p) and os.path.exists(power_k):
        print "Looking at output files..."
        print "    " + power_k
        print "    " + power_p
        print "    using cached files."
        ps = (
            numpy.loadtxt(power_k),
            numpy.loadtxt(power_p))

    else:

        positions = []

        box_l = None

        for f_comps in files:
            sim = pynbody.load(os.path.join(*f_comps))
            sim.physical_units("Mpc")
            if box_l is None:
                # should be the same for all files, even if split simulation
                # we could also derive this from function param size...
                box_l = sim.properties["boxsize"].in_units(
                    "Mpc")*sim.properties["h"]
            positions.append(sim.dm["pos"])

        np_positions = numpy.vstack(positions)


        # proper to comoving
        a_value = 1.0/(1.0 + z_value)
        np_positions /= a_value
        # also convert box_l??
        box_l /= a_value



        print "Looking at output files..."
        print "    " + power_k
        print "    " + power_p
        print "    caching..."
        dens = power_spectrum.part2dens3d(np_positions, box_l, bins_amounts(size))
        overdens = power_spectrum.dens2overdens(dens)
        ps = list(reversed(
            power_spectrum.power_spectrum(overdens, box_l, bin_k=bins_amounts(size))))

        basedir = os.path.dirname(power_base)
        if not os.path.exists(basedir):
            os.makedirs(basedir)
        numpy.savetxt(power_k, ps[0])
        numpy.savetxt(power_p, ps[1])

        print "    done.\n"


    return ps, nearest_z



def getPowerSpectrum_Gadget(z_value, size, bins):
    return getPowerSpectrum_Sim(getDataFiles_Gadget, z_value, size, bins)
def getPowerSpectrum_LPicola(z_value, size, bins):
    return getPowerSpectrum_Sim(getDataFiles_LPicola, z_value, size, bins)
def getPowerSpectrum_LPicola_nmesh512(z_value, size, bins):
    return getPowerSpectrum_Sim(getDataFiles_LPicola_nmesh512, z_value, size, bins)
def getPowerSpectrum_LPicola_nmesh1024(z_value, size, bins):
    return getPowerSpectrum_Sim(getDataFiles_LPicola_nmesh1024, z_value, size, bins)

def getPowerSpectrum_Ref(z_value):
    base_path = "L-PICOLA/Quentin/theory_w_gadget_params"
    fname = "pk_nonlin_z%s.npy" % ("%.3f" % z_value).replace(".", "p")

    npy_file = os.path.join(*cacheSMBFile(base_path, fname))

    data = numpy.load(npy_file)
    return (data[:, 0], data[:, 1]), z_value










def main():
    for z_value in Z_VALUES:

        fig = plt.figure(figsize=(15, 15))

        items = BOXES.items()
        total_items = sum(len(x[1]) for x in items)

        main_axis = fig.add_subplot(111)
        #http://stackoverflow.com/questions/6963035/pyplot-axes-labels-for-subplots
        main_axis.spines["top"].set_color("none")
        main_axis.spines["bottom"].set_color("none")
        main_axis.spines["left"].set_color("none")
        main_axis.spines["right"].set_color("none")
        main_axis.tick_params(
            labelcolor="w", top="off", bottom="off", left="off", right="off")

        plt.setp(main_axis.get_xticklabels(), visible=False)
        plt.setp(main_axis.get_yticklabels(), visible=False)


        #main_axis.set_title(
        #    "Power Spectrum for $z = %.3f$" % z_value,
        #    y=1.06, fontsize=25)
        # TODO: use usetex, amsmath (\text) and/or siunitx
        main_axis.set_xlabel(
            r"$k$ ($h/\mathrm{Mpc}$)",
            labelpad=30.0, fontsize=16)
        main_axis.set_ylabel(
            r"Power $P(k)$ ($(\mathrm{Mpc}/h)^3$)",
            labelpad=35.0, fontsize=16)

        fig.subplots_adjust(wspace=0.1, hspace=0.2)

        y_amount = int(math.ceil(total_items**0.5))
        x_amount = int(math.ceil(float(total_items)/y_amount))
        i = 1

        for size, bins_list in items:
            for bins in bins_list:
                # TODO: shares
                ax = fig.add_subplot(y_amount, x_amount, i)
                i += 1

                ps_lpicola_512, z_lpicola_512 = getPowerSpectrum_LPicola_nmesh512(
                    z_value, size, bins)
                ps_gadget, z_gadget = getPowerSpectrum_Gadget(
                    z_value, size, bins)
                ps_lpicola, z_lpicola = getPowerSpectrum_LPicola(
                    z_value, size, bins)
                #ps_lpicola_1024, z_lpicola_1024 = getPowerSpectrum_LPicola_nmesh1024(
                #    z_value, size, bins)

                if len(ps_gadget[0]) and len(ps_lpicola[0]):
                    # XXX: why do we need *2 here???
                    min_k = min(ps_gadget[0][0], ps_lpicola[0][0], ps_lpicola_512[0][0])*2
                    max_k = max(ps_gadget[0][-1], ps_lpicola[0][-1], ps_lpicola_512[0][-1])*2
                    min_p = min(
                        min(v for v in ps_gadget[1] if not numpy.isnan(v)),
                        min(v for v in ps_lpicola[1] if not numpy.isnan(v)),
                        min(v for v in ps_lpicola_512[1] if not numpy.isnan(v)),
                        #min(v for v in ps_lpicola_1024[1] if not numpy.isnan(v))
                        )/2
                    max_p = max(
                        max(v for v in ps_gadget[1] if not numpy.isnan(v)),
                        max(v for v in ps_lpicola[1] if not numpy.isnan(v)),
                        max(v for v in ps_lpicola_512[1] if not numpy.isnan(v)),
                        #max(v for v in ps_lpicola_1024[1] if not numpy.isnan(v))
                        )*2

                elif len(ps_gadget[0]):
                    # TODO: ps_gadget_512?
                    min_k = min(ps_gadget[0][0]*2, ps_gadget_512[0][0]*2, ps_gadget_1024[0][0]*2)
                    max_k = max(ps_gadget[0][-1]*2, ps_gadget_512[0][-1]*2, ps_gadget_1024[0][-1]*2)
                    min_p = min(v for v in ps_gadget[1] if not numpy.isnan(v))/2
                    max_p = max(v for v in ps_gadget[1] if not numpy.isnan(v))*2

                elif len(ps_lpicola[0]):
                    min_k = ps_lpicola[0][0]*2
                    max_k = ps_lpicola[0][-1]*2
                    min_p = min(v for v in ps_lpicola[1] if not numpy.isnan(v))/2
                    max_p = max(v for v in ps_lpicola[1] if not numpy.isnan(v))*2

                # XXX: any nicer way?
                
                ps_ref, z_ref = getPowerSpectrum_Ref(z_value)

                if len(ps_ref[0]):
                    plt.loglog(
                        *ps_ref,
                        label=r"$P_\mathrm{Theory}$")
                if len(ps_gadget[0]):
                    plt.loglog(
                        *ps_gadget,
                        label=r"$P_\mathrm{Gadget}$")
                if len(ps_lpicola[0]):
                    plt.loglog(
                        *ps_lpicola,
                        label=r"$P_{\mathrm{PICOLA,} N_\mathrm{mesh} = %s}$" % bins)
                if len(ps_lpicola_512[0]):
                    plt.loglog(
                        *ps_lpicola_512,
                        label=r"$P_{\mathrm{PICOLA,} N_\mathrm{mesh} = 512}$")
                #if len(ps_lpicola_1024[0]):
                #    plt.loglog(
                #        *ps_lpicola_1024,
                #        label=r"$P_{\mathrm{PICOLA,} N_\mathrm{mesh} = 1024}$")


                ax.set_title(r"$\mathrm{%s}/h$, $N_\mathrm{part} = %s$" % (size, bins))
                ax.set_xlim(min_k, max_k)
                ax.set_ylim(min_p, max_p)
                ax.legend(loc="lower left")


        plt.savefig("power_spectra_all_nmesh512_%s.pdf" % (
            ("%.3f" % z_value).replace(".", "p")))
        #plt.show()
        print "-----------------"



if __name__ == "__main__":
    main()
