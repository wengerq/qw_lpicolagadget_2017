# ##### BEGIN GPL LICENSE BLOCK #####
#
#    draw_det.py - Draw ZA/2LPT determinants from value tables
#    Copyright (C) 2017  Quentin Wenger
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####


import csv
import os

import matplotlib.pyplot as plt



in_path = "./output_det_0p000000_9p000000_0p100000_0p276000_0p724000.csv"
out_path = "./dets.pdf"




zs = []
as_ = []
dis = []
di2s = []
dvs = []
dv2s = []
dets = []

cols = [zs, as_, dis, di2s, dvs, dv2s, dets]

with open(in_path, "r") as fo:
    first = True
    for row in csv.reader(fo):
        if first:
            first = False
            continue
        for a, b in zip(row, cols):
            b.append(a)


#plt.title(
#    r"ZA/2LPT Reconstruction Determinant, "\
#    r"$\Omega_0 = 0.276, \Omega_\Lambda = 0.724$")
plt.xlabel(r"$z$")
plt.ylabel(r"$\Delta = D_i D_{v2} - D_v D_{i2}$")

plt.plot(zs, dets)
plt.savefig(out_path)
