/* ==========================================================================*/
/*   Version 1.2.             Cullan Howlett & Marc Manera,                  */
/*   Copyright (c) 2015       Institute of Cosmology and Gravitation         */
/*                            (University of Portsmouth) & University        */
/*                            College London.                                */
/*                                                                           */
/*   This file is part of L-PICOLA.                                          */
/*                                                                           */
/*   L-PICOLA is free software: you can redistribute it and/or modify        */
/*   it under the terms of the GNU General Public License as published by    */
/*   the Free Software Foundation, either version 3 of the License, or       */
/*   (at your option) any later version.                                     */
/*                                                                           */
/*   L-PICOLA is distributed in the hope that it will be useful,             */
/*   but WITHOUT ANY WARRANTY; without even the implied warranty of          */
/*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           */
/*   GNU General Public License for more details.                            */
/*                                                                           */
/*   You should have received a copy of the GNU General Public License       */
/*   along with L-PICOLA.  If not, see <http://www.gnu.org/licenses/>.       */
/* ==========================================================================*/

/* ======================================================*/
/* This file contains all the cosmology related routines.*/
/* v1.1: New routines for calculating the second-order   */
/*       growth factor and associated derivatives        */
/* ======================================================*/


/*QW: modified from L-PICOLA source*/

#include <time.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// GSL libraries
#include <gsl/gsl_rng.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_roots.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_sf_hyperg.h> 
#include <gsl/gsl_sort_double.h>
#include <gsl/gsl_integration.h>



#include "proto.h"

double Omega;
double OmegaLambda;




// The Hubble expansion
// ====================
double Hubblea(double a) { 
  return sqrt(Omega/(a*a*a)+OmegaLambda+(1.0-Omega-OmegaLambda)/(a*a));
}

// The Q factor from Tassev et. al., 2013 (Q = a^3 H(a)/H0)
// ========================================================
double Qfactor(double a) { 
  return Hubblea(a)*a*a*a;
}

// Normalised growth factor for LCDM
// =================================
double growthD(double a){ 
  return Hubblea(a)*growthDtemp(a)/growthDtemp(1.0);
}

// Growth factor integral
// ======================
double growthDtemp(double a) {

  double alpha = 0.0;
  double result, error;

  gsl_function F;
  gsl_integration_workspace * w = gsl_integration_workspace_alloc (5000);
     
  F.function = &growthDtempfunc;
  F.params = &alpha;
     
  gsl_integration_qag(&F,0.0,a,0,1e-5,5000,6,w,&result,&error); 
      
  gsl_integration_workspace_free (w);
     
  return result;
}

// Growth factor integrand
// =======================
double growthDtempfunc(double a, void * params) {       
  return 1.0/(pow(a*Hubblea(a),3.0));
}

// Normalised Second order growth factor
// =====================================
double growthD2(double a) { 
  return growthD2temp(a)/growthD2temp(1.0);
}

// Second order growth factor (we use Matsubara 1995 as this works for non-flat cosmology)
// =======================================================================================
double growthD2temp(double a) {
  double D1 = growthD(a);
  double Om = Omega/(Omega+OmegaLambda*a*a*a+(1.0-Omega-OmegaLambda)*a);
  double Ol = OmegaLambda/(Omega/(a*a*a)+OmegaLambda+(1.0-Omega-OmegaLambda)/(a*a));
  double u32fac = u32(a);
  double u52fac = u52(a);
  return (7.0/3.0)*D1*D1*(Om/4.0-Ol/2.0-((1.0-((3.0*u52fac)/(2.0*u32fac)))/u32fac));
}

// The function U_{3/2} in Matsubara 1995
// ======================================
double u32(double a) {

  double alpha = 0.0;
  double result, error;

  gsl_function F;
  gsl_integration_workspace * w = gsl_integration_workspace_alloc (5000);
     
  F.function = &u32func;
  F.params = &alpha;
     
  gsl_integration_qag(&F,0.0,a,0,1e-5,5000,6,w,&result,&error); 
      
  gsl_integration_workspace_free (w);
     
  double ea = Hubblea(a);

  return a*a*ea*ea*ea*result;
}

// The integrand for the function U_{3/2} in Matsubara 1995
// ========================================================
double u32func(double a, void * params) {       
  return 1.0/(pow(a*Hubblea(a),3.0));
}

// The function U_{5/2} in Matsubara 1995
// ======================================
double u52(double a) {

  double alpha = 0.0;
  double result, error;

  gsl_function F;
  gsl_integration_workspace * w = gsl_integration_workspace_alloc (5000);
     
  F.function = &u52func;
  F.params = &alpha;
     
  gsl_integration_qag(&F,0.0,a,0,1e-5,5000,6,w,&result,&error); 
      
  gsl_integration_workspace_free (w);
     
  double ea = Hubblea(a);

  return a*a*a*a*ea*ea*ea*ea*ea*result;
}

// The integrand for the function U_{5/2} in Matsubara 1995
// ========================================================
double u52func(double a, void * params) {       
  return 1.0/(pow(a*Hubblea(a),5.0));
}

// returns Q*d(D_{1})/da, where Q=Qfactor(a) 
// =========================================
double QdD1da(double a) {
  return (1.0/Hubblea(a))*((1.0/growthDtemp(1.0))-((((3.0*Omega)/(2.0*a))+(1.0-Omega-OmegaLambda))*growthD(a)));
}

// returns Q*d(D_{2})/da, where Q=Qfactor(a) 
// =========================================
double QdD2da(double a) {
  double D1 = growthD(a);
  double Om = Omega/(Omega+OmegaLambda*a*a*a+(1.0-Omega-OmegaLambda)*a);
  double Ol = OmegaLambda/(Omega/(a*a*a)+OmegaLambda+(1.0-Omega-OmegaLambda)/(a*a));
  double u32fac = u32(a);
  double u52fac = u52(a);
  double factor = -3.0*Om+((2.0+4.0*u32fac-3.0*(2.0+Om-2.0*Ol)*u52fac)/(u32fac*u32fac));
  return (7.0/12.0)*D1*D1*a*a*Hubblea(a)*factor;
}







int main(int argc, char **argv) {
  
  FILE * fp;
  char buf[300];
  double Di, Di2, Dv, Dv2;
  double z0, z1, dz, z, A;
  char *ci;
  char point[] = ".";
  
  // args: z0, z1, dz, Omega, OmegaLambda
  
  if (argc < 6) {
    printf("\nERROR: Not enough arguments. Expected z0, z1, dz, Omega, OmegaLambda.\n\n");
    fflush(stdout);
    return 1;
  }
  
  z0 = atof(argv[1]);
  z1 = atof(argv[2]);
  dz = atof(argv[3]);
  Omega = atof(argv[4]);
  OmegaLambda = atof(argv[5]);
  
  
  sprintf(buf, "output_det_%f_%f_%f_%f_%f", z0, z1, dz, Omega, OmegaLambda);
  ci = strpbrk(buf, point);
  while (ci != NULL)
  {
    *ci = 'p';
    ci = strpbrk(ci + 1, point);
  }
  strcat(buf, ".csv");
  
  if(!(fp = fopen(buf, "w"))) {
    printf("\nERROR: Can't open output file '%s'.\n\n", buf);
    fflush(stdout);
    return 1;
  }
  
  fprintf(fp, "z,a,Di,Di2,Dv,Dv2,det\n");
  
  for (z = z0; z <= z1; z += dz) {
    
    A = 1.0/(1.0 + z);
    
    Di = growthD(A);
    Di2 = growthD2(A);
    Dv = QdD1da(A);
    Dv2 = QdD2da(A);
    
    fprintf(fp, "%f,%f,%f,%f,%f,%f,%f\n", z, A, Di, Di2, Dv, Dv2, Di*Dv2 - Dv*Di2);
  }
  
  fclose(fp);
  
  return 0;
}




