/* ==========================================================================*/
/*   Version 1.2.             Cullan Howlett & Marc Manera,                  */
/*   Copyright (c) 2015       Institute of Cosmology and Gravitation         */
/*                            (University of Portsmouth) & University        */
/*                            College London.                                */
/*                                                                           */
/*   This file is part of L-PICOLA.                                          */
/*                                                                           */
/*   L-PICOLA is free software: you can redistribute it and/or modify        */
/*   it under the terms of the GNU General Public License as published by    */
/*   the Free Software Foundation, either version 3 of the License, or       */
/*   (at your option) any later version.                                     */
/*                                                                           */
/*   L-PICOLA is distributed in the hope that it will be useful,             */
/*   but WITHOUT ANY WARRANTY; without even the implied warranty of          */
/*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           */
/*   GNU General Public License for more details.                            */
/*                                                                           */
/*   You should have received a copy of the GNU General Public License       */
/*   along with L-PICOLA.  If not, see <http://www.gnu.org/licenses/>.       */
/* ==========================================================================*/

/* =====================================================================*/
/* This file contains all the prototypes for function used in the code. */
/* v1.2: Updated prototypes for Output and Output_Info corresponding to */
/*       z=9 bug fix in main.c                                          */
/* =====================================================================*/

/*QW: modified from L-PICOLA source*/

double u32(double a);
double u52(double a);
double QdD1da(double a);
double QdD2da(double a);
double Hubblea(double a);
double Qfactor(double a);
double growthD(double a);
double growthD2(double a);
double growthDtemp(double a);
double growthD2temp(double a);
double u32func(double a, void * params);
double u52func(double a, void * params);
double growthDtempfunc(double a, void * params);

