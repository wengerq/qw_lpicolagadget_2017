# ##### BEGIN GPL LICENSE BLOCK #####
#
#    draw_ratios.py - Draw PS ratios as function of z
#    Copyright (C) 2017  Quentin Wenger
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####


import matplotlib.pyplot as plt
import numpy



in_path = "./ratios.txt"
out_path = "./ratios.pdf"


ratios = numpy.loadtxt(in_path)
plt.plot(ratios[:, 0], ratios[:, 1], "o-")

plt.xlim(9, 0)
plt.ylim(0, 1)
#plt.title("Mean ratio L-PICOLA mod/L-PICOLA")
plt.xlabel("$z$")
plt.ylabel("ratio")

plt.savefig(out_path)
