# ##### BEGIN GPL LICENSE BLOCK #####
#
#    compare_lpicola.py - Draw PS of unmodified and modified L-PICOLA
#    runs from same initial conditions, and compute ratios
#    Copyright (C) 2017  Quentin Wenger
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####


import os
import sys
import re
import math
import itertools
import collections
import tempfile
import shutil

import matplotlib.pyplot as plt
import numpy


sys.path.append("../..")
from qw_analyse_gadget.utils.pynbody_custom import pynbody

from qw_analyse_gadget.analysis import power_spectrum
from qw_analyse_gadget.analysis import probability_density_function
from qw_analyse_gadget.analysis import halo_mass_function
from qw_analyse_gadget.analysis import manual_density



do_img = False

do_pdf = False

do_hmf = False

do_ps = True
do_psr = False



# for halo calculation
#eps = 0.01#0.001

#cs
dt = 200

# for img
#slice_bounds = [30.0, 33.0]
#slice_index = 2




picbins = 8


lpicola_output = "/some/path/lpicola_local_run/output/"
# not a bug, gadget stays at the same value as for 6Gpc
lpicola_mod_output = "/some/path/lpicola_local_run/output_mod/"
out_dir = "./"


files = []

files.append((
    lpicola_output + "out_z9p000_stacked",
    lpicola_mod_output + "out_z9p000_stacked"))
"""
files.append((
    lpicola_output + "out_z8p000_stacked",
    lpicola_mod_output + "out_z8p000_stacked"))
files.append((
    lpicola_output + "out_z7p000_stacked",
    lpicola_mod_output + "out_z7p000_stacked"))
"""
files.append((
    lpicola_output + "out_z6p000_stacked",
    lpicola_mod_output + "out_z6p000_stacked"))
"""
files.append((
    lpicola_output + "out_z5p000_stacked",
    lpicola_mod_output + "out_z5p000_stacked"))
files.append((
    lpicola_output + "out_z4p000_stacked",
    lpicola_mod_output + "out_z4p000_stacked"))
files.append((
    lpicola_output + "out_z3p000_stacked",
    lpicola_mod_output + "out_z3p000_stacked"))
"""
files.append((
    lpicola_output + "out_z1p997_stacked",
    lpicola_mod_output + "out_z1p997_stacked"))
"""
files.append((
    lpicola_output + "out_z1p488_stacked",
    lpicola_mod_output + "out_z1p488_stacked"))
"""
"""
files.append((
    lpicola_output + "out_z0p511_stacked",
    lpicola_mod_output + "out_z0p511_stacked"))
"""
"""
files.append((
    lpicola_output + "out_z0p211_stacked",
    lpicola_mod_output + "out_z0p211_stacked"))
files.append((
    lpicola_output + "out_z0p103_stacked",
    lpicola_mod_output + "out_z0p103_stacked"))
"""
files.append((
    lpicola_output + "out_z0p000_stacked",
    lpicola_mod_output + "out_z0p000_stacked"))




def getPowerSpectrum_Ref(z_value):
    base_path = "/some/path/downloads/L-PICOLA/"\
        "Quentin/theory_w_gadget_params"
    fname = "pk_nonlin_z%s.npy" % ("%.3f" % z_value).replace(".", "p")

    npy_file = os.path.join(base_path, fname)

    # XXXX: temp
    if os.path.exists(npy_file):
        data = numpy.load(npy_file)
        return (data[:, 0], data[:, 1])
    else:
        return (numpy.zeros((2,)) + 1e-10, numpy.zeros((2,)) + 1e-10)


"""
def getFileNameBase(files):
    # we can assume all files to be in the same dir
    dir_path = os.path.dirname(files[0])
    # inspired by http://stackoverflow.com/questions/18715688/find-common-substring-between-two-strings
    def _iter():
        for chars in zip(*[os.path.basename(f) for f in files]):
            if len(set(chars)) == 1:
                yield chars[0]
            else:
                return


    common_name = ''.join(_iter())
    if not common_name:
        # XXX: anything better???
        common_name = "dummy"

    return os.path.join(dir_path, common_name)
"""


def compute_dens(pos, box_l, bins):
    return power_spectrum.part2dens3d(pos, box_l, bins)

def compute_pdf(dens, bins_width):
    return probability_density_function.pdf_memoryfriendly_bins(
        dens, bins_width, True)

def compute_ps(dens, box_l, bins):
    overdens = power_spectrum.dens2overdens(dens)
    return list(reversed(
        power_spectrum.power_spectrum(overdens, box_l, bin_k=bins)))



def plot_double(title, xlabel, ylabel):
    fig = plt.figure(figsize=(22, 10))
    main_axis = fig.add_subplot(111)
    main_axis.spines["top"].set_color("none")
    main_axis.spines["bottom"].set_color("none")
    main_axis.spines["left"].set_color("none")
    main_axis.spines["right"].set_color("none")
    main_axis.tick_params(
        labelcolor="w", top="off", bottom="off", left="off", right="off")

    plt.setp(main_axis.get_xticklabels(), visible=False)
    plt.setp(main_axis.get_yticklabels(), visible=False)

    main_axis.set_title(title, fontsize=25, y=1.03)
    main_axis.set_xlabel(xlabel, labelpad=30.0, fontsize=16)
    main_axis.set_ylabel(ylabel, labelpad=35.0, fontsize=16)

    ax1 = fig.add_subplot(1, 2, 1)
    ax2 = fig.add_subplot(1, 2, 2)

    fig.subplots_adjust(wspace=0.1, hspace=0.1)

    return ax1, ax2



def plot_single(title, xlabel, ylabel):
    fig = plt.figure(figsize=(16, 10))
    main_axis = fig.add_subplot(111)
    main_axis.set_title(title, fontsize=25, y=1.03)
    main_axis.set_xlabel(xlabel, labelpad=30.0, fontsize=16)
    main_axis.set_ylabel(ylabel, labelpad=35.0, fontsize=16)
    return main_axis


out_path_img = out_dir + "img_%.3f.png"
out_path_img_gif = out_dir + "imgs_all.gif"

out_path_pdf = out_dir + "pdf_1Mpcbin_%.3f.png"
out_path_pdf_gif = out_dir + "pdfs_1Mpcbin_all.gif"

out_path_hmf = out_dir + "hmf_%.3f.png"
out_path_hmf_gif = out_dir + "hmfs_all.gif"

out_path_ps = out_dir + "ps_1Mpcbin_%.3f.png"
out_path_ps_gif = out_dir + "pss_1Mpcbin_all.gif"

out_path_psr = out_dir + "ps_ratio_1Mpcbin_%.3f.png"
out_path_psr_gif = out_dir + "pss_ratio_1Mpcbin_all.gif"


ratios_path = out_dir + "ratios.txt"
#dens_path = "./temp/6Gpc_zinit9/dens.npy"
# again not a bug
#dens2_path = "./temp/6Gpc/dens2.npy"


def plot_img(
    pos_img, pos_img2, label, label2, xmin, xmax, ymin, ymax,
    title, xlabel, ylabel, lpicola_z
    ):
    ax1, ax2 = plot_double(title, xlabel, ylabel)

    # XXX: make sure this takes the whole box width/height
    ax1.scatter(pos_img2[:, 0], pos_img2[:, 1], edgecolors=None, s=1.0)
    ax1.set_xlim(xmin, xmax)
    ax1.set_ylim(ymin, ymax)
    ax1.set_title(label2)

    ax2.scatter(pos_img[:, 0], pos_img[:, 1], edgecolors=None, s=1.0)
    ax2.set_xlim(xmin, xmax)
    ax2.set_ylim(ymin, ymax)
    ax2.set_title(label)

    path = out_path_img % lpicola_z
    plt.savefig(path)
    print "saved IMG z = " + str(lpicola_z)
    return path


def plot_pdf(
    pdf, pdf2, label, label2, xmin, xmax, ymin, ymax,
    title, xlabel, ylabel, lpicola_z, bins_width
    ):
    main_axis = plot_single(title, xlabel, ylabel)
    main_axis.bar(
        pdf[1], pdf[0], width=bins_width, label=label,
        color=(1.0, 0.0, 0.0, 0.5))
    main_axis.bar(
        pdf2[1], pdf2[0], width=bins_width, label=label2,
        color=(0.0, 0.0, 1.0, 0.5))
    main_axis.set_xlim(xmin, xmax)
    main_axis.set_ylim(ymin, ymax)
    plt.legend(loc="upper right")
    path = out_path_pdf % lpicola_z
    plt.savefig(path)
    print "saved PDF z = " + str(lpicola_z)
    return path

"""
def plot_hmf(
    hmf, hmf2, label, label2, xmin, xmax, ymin, ymax,
    title, xlabel, ylabel, lpicola_z
    ):
    main_axis = plot_single(title, xlabel, ylabel)
    main_axis.loglog(hmf[1], hmf[0], label=label)
    main_axis.loglog(hmf2[1], hmf2[0], label=label2)
    main_axis.set_xlim(xmin, xmax)
    main_axis.set_ylim(ymin, ymax)
    plt.legend(loc="upper right")
    path = out_path_hmf % lpicola_z
    plt.savefig(path)
    print "saved HMF z = " + str(lpicola_z)
    return path
"""

def plot_ps(
    ax,
    ps, ps2, ps_ref, label, label2, label_ref, xmin, xmax, ymin, ymax,
    title, xlabel, ylabel, lpicola_z
    ):
    ax.loglog(*ps_ref, label=label_ref)
    ax.loglog(*ps, label=label)
    ax.loglog(*ps2, label=label2)
    ax.set_xlim(xmin, xmax)
    ax.set_ylim(ymin, ymax)
    ax.set_title(r"$z = %.3f$" % lpicola_z)
    plt.legend(loc="lower left")
    """
    path = out_path_ps % lpicola_z
    plt.savefig(path)
    """
    print "PS z = " + str(lpicola_z)
    return# path


def plot_psr(
    ps, ps2, ps_ref, label, label_ref, xmin, xmax, ymin, ymax,
    title, xlabel, ylabel, lpicola_z
    ):

    ps_ref_range = [[], []]
    for k, _ in zip(*ps):
        i = (numpy.abs(ps_ref[0] - k)).argmin()
        ps_ref_range[0].append(ps_ref[0][i])
        ps_ref_range[1].append(ps_ref[1][i])

    assert (ps[0] == ps2[0]).all()

    main_axis = plot_single(title, xlabel, ylabel)
    main_axis.plot([xmin, xmax], [1.0, 1.0], "--", color="black")
    main_axis.plot(ps[0], ps_ref_range[1] / ps[1], label=label_ref)
    main_axis.plot(ps[0], ps2[1] / ps[1], label=label)
    main_axis.set_xscale("log")
    main_axis.set_xlim(xmin, xmax)
    main_axis.set_ylim(ymin, ymax)
    plt.legend(loc="upper right")
    path = out_path_psr % lpicola_z
    plt.savefig(path)
    print "saved PSR z = " + str(lpicola_z)
    return path


paths_img = []
paths_pdf = []

paths_hmf = []

paths_ps = []
paths_psr = []



if __name__ == "__main__":

    """
    halos_tmpdir = tempfile.mkdtemp()
    """

    ratios = numpy.zeros((0, 2))

    fig = plt.figure(figsize=(15, 20))

    main_axis = fig.add_subplot(111)
    #http://stackoverflow.com/questions/6963035/pyplot-axes-labels-for-subplots
    main_axis.spines["top"].set_color("none")
    main_axis.spines["bottom"].set_color("none")
    main_axis.spines["left"].set_color("none")
    main_axis.spines["right"].set_color("none")
    main_axis.tick_params(
        labelcolor="w", top="off", bottom="off", left="off", right="off")

    plt.setp(main_axis.get_xticklabels(), visible=False)
    plt.setp(main_axis.get_yticklabels(), visible=False)


    #main_axis.set_title(
    #    "Power Spectrum for $z = %.3f$ (smoothing: 10Mpc)" % z_value,
    #    y=1.06, fontsize=25)
    # TODO: use usetex, amsmath (\text) and/or siunitx
    main_axis.set_xlabel(
        r"$k$ ($a^{-1} \cdot h \cdot \mathrm{Mpc}^{-1}$)",
        labelpad=30.0, fontsize=16)
    main_axis.set_ylabel(
        r"Power $P(k)$ ($(a \cdot \mathrm{Mpc}/h)^3$)",
        labelpad=35.0, fontsize=16)

    fig.subplots_adjust(wspace=0.1, hspace=0.2)


    j = 1
    for lpicola_file, lpicola_mod_file in files:
        ax = fig.add_subplot(len(files), 1, j)
        j += 1
        """
        sims = []
        for lpicola_file in lpicola_files:
            sims.append(pynbody.load(lpicola_file))
            sims[-1].physical_units("Mpc a h^-1", mass="Msol h^-1")
        sim = pynbody.new(sum([len(s) for s in sims]))

        sim_path = os.path.join(
            halos_tmpdir,
            os.path.basename(getFileNameBase(lpicola_files)))

        sim.properties = sims[0].properties
        sim["pos"].units = sims[0]["pos"].units
        sim["vel"].units = sims[0]["vel"].units

        sim.write(
            fmt=pynbody.snapshot.gadget.GadgetSnap,
            filename=sim_path,
            format2=False)
        sim = pynbody.load(sim_path)
        sim.physical_units("Mpc a h^-1", mass="Msol h^-1")

        i = 0
        for s in sims:
            le = len(s)
            sim["vel"][i:i + le] = s["vel"]
            sim["pos"][i:i + le] = s["pos"]
            # XXX: integer size problems????
            # XXX: -> try s["iord"].astype(numpy.uint32)
            #if "iord" in s.all_keys():
            #    sim["iord"][i:i + le] = s["iord"]
            i += le

        # if particle indices are not available in L-PICOLA derived snaps
        if (sim["iord"] == 0).all():
            sim["iord"][:] = numpy.arange(len(sim["iord"]))

        # has to save/load twice, in order to have a header bound to the sim,
        # which can them be set to the wanted parameters
        sim.header = sims[0].header
        sim.header.npart = numpy.sum(
            numpy.vstack([s.header.npart for s in sims]), axis=0)
        sim.header.npartTotal = sim.header.npart
        sim.header.num_files = 1

        sim.write(
            fmt=pynbody.snapshot.gadget.GadgetSnap,
            filename=sim_path,
            format2=False)
        """
        sim = pynbody.load(lpicola_file)
        #sim.physical_units("Mpc a h^-1", mass="Msol h**-1")

        sim2 = pynbody.load(lpicola_mod_file)
        #sim2.physical_units("Mpc a h^-1", mass="Msol h**-1")

        # XXX: for some reason hmf computing does not work with those,
        # so set them _after_ the hmf calculation...
        #sim.physical_units("Mpc a h**-1", mass="Msol h^-1")
        #sim2.physical_units("Mpc a h**-1", mass="Msol h^-1")

        if do_hmf:
            # actually the "ignore_ids" is not needed anymore... but we don't need
            # ids either.
            halos = sim.halos(
                eps=eps, filename=sim_path + "_halos", ignore_ids=True)
            halos2 = sim2.halos(
                eps=eps, filename=os.path.join(
                    halos_tmpdir, os.path.basename(gadget_file) + "_halos"),
                ignore_ids=True)


        a_value = sim.properties["a"]
        lpicola_z = 1.0/a_value - 1.0

        sim.physical_units("Mpc a h**-1", mass="Msol h**-1")
        sim2.physical_units("Mpc a h**-1", mass="Msol h**-1")
        
        pos = sim.dm["pos"]
        pos2 = sim2.dm["pos"]

        box_l = sim2.properties["boxsize"].in_units("Mpc a h**-1")
        h_value = sim2.properties["h"]

        #bins = int(round(box_l / 10.0))
        #bins = 128
        bins = int(round(box_l / 1.0))

        bins_width = 1

        step = 1.1

        max_pos = 10**8


        if do_pdf or do_ps or do_psr:
            dens = compute_dens(pos, box_l, bins)
            dens2 = compute_dens(pos2, box_l, bins)

        """
        # TODO
        if do_img:
            # better/np way?
            pos_img = numpy.vstack([
                p for p in pos.in_units("Mpc a h**-1")
                if slice_bounds[0] <= p[2] <= slice_bounds[1]])[:, :2]
            # better/np way?
            pos_img2 = numpy.vstack([
                p for p in pos2.in_units("Mpc a h**-1")
                if slice_bounds[0] <= p[2] <= slice_bounds[1]])[:, :2]
        """


        if do_ps or do_psr:
            ps = compute_ps(dens, box_l, bins)
            ps2 = compute_ps(dens2, box_l, bins)
                
            ps_ref = getPowerSpectrum_Ref(lpicola_z)
            # ref is internally in comoving coords - a Mpc
            # just have to divide by h
            ps_ref = (ps_ref[0]/h_value, ps_ref[1]*h_value**3)

            a = []
            b = []
            for p2, p in zip(ps2[1], ps[1]):
                if not numpy.isnan(p) and not numpy.isnan(p2):
                    a.append(p2)
                    b.append(p)
            
            ratios = numpy.vstack([ratios, [
                lpicola_z, numpy.mean(numpy.array(a)/numpy.array(b))]])

        """
        if do_img:
            paths_img.append(plot_img(
                pos_img=pos_img, pos_img2=pos_img2,
                label="L-PICOLA", label2="GADGET",
                xmin=0.0, xmax=100.0, ymin=0.0, ymax=100.0,
                title=r"Box slice, 100Mpc/h, 128 bins, $z = %.3f$" % lpicola_z,
                xlabel=r"$x$ ($a \cdot \mathrm{Mpc} \cdot h^{-1}$)",
                ylabel=r"$y$ ($a \cdot \mathrm{Mpc} \cdot h^{-1}$)",
                lpicola_z=lpicola_z))
        """

        if do_pdf:
            pdf = compute_pdf(dens, bins_width)
            pdf2 = compute_pdf(dens2, bins_width)

            paths_pdf.append(plot_pdf(
                pdf=pdf, pdf2=pdf2,
                label="L-PICOLA", label2="GADGET",
                xmin=0, xmax=20, ymin=0.0, ymax=1.0,
                title=r"normalized PDF, 100Mpc/h, 128 bins, 1Mpc-density-binning, $z = %.3f$" % lpicola_z,
                xlabel=r"Particles per cell",
                ylabel=r"Occurrences",
                lpicola_z=lpicola_z,
                bins_width=bins_width))

        """
        if do_hmf:
            if len(halos) == 0:
                hmf = ([], [])
            else:
                ms = numpy.stack([h["mass"].sum() for h in halos])
                hmf = halo_mass_function.hmf(ms, step)
            if len(halos2) == 0:
                hmf2 = ([], [])
            else:
                ms = numpy.stack([h["mass"].sum() for h in halos2])
                hmf2 = halo_mass_function.hmf(ms, step)

            paths_hmf.append(plot_hmf(
                hmf=hmf, hmf2=hmf2,
                label="L-PICOLA" + ("" if len(halos) else " (void)"),
                label2="GADGET" + ("" if len(halos2) else " (void)"),
                xmin=1e12, xmax=1e15, ymin=1e-4, ymax=1e0,
                title=r"HMS, 100Mpc/h, 128 bins, $z = %.3f$, $\epsilon = %.3f$" % (lpicola_z, eps),
                xlabel=r"$M$ ($h^{-1} M_{\odot}$)",
                ylabel=r"$\mathrm{d} n / \mathrm{d} \log M$ ($h^3 \mathrm{Mpc}^{-3}$)",
                lpicola_z=lpicola_z))
        """

        if do_ps:
            paths_ps.append(plot_ps(
                ax,
                ps=ps, ps2=ps2, ps_ref=ps_ref,
                label="L-PICOLA", label2="L-PICOLA mod", label_ref="Theory",
                xmin=1e-1, xmax=1e1, ymin=1e-1, ymax=1e4,
                title=r"PS, 100Mpc/h, 128 bins, 1Mpc-density-binning, $z = %.3f$" % lpicola_z,
                xlabel=r"$k$ ($a^{-1} \cdot h \cdot \mathrm{Mpc}^{-1}$)",
                ylabel=r"$P_k$ ($(a \cdot \mathrm{Mpc} / h)^3$)",
                lpicola_z=lpicola_z))

        if do_psr:
            paths_psr.append(plot_psr(
                ps=ps, ps2=ps2, ps_ref=ps_ref,
                label="L-PICOLA mod/L-PICOLA", label_ref="Theory/L-PICOLA",
                xmin=1e-1, xmax=1e1, ymin=0.0, ymax=2.0,
                title=r"PS ratio, 100Mpc/h, 128 bins, 1Mpc-density-binning, $z = %.3f$" % lpicola_z,
                xlabel=r"$k$ ($a^{-1} \cdot h \cdot \mathrm{Mpc}^{-1}$)",
                ylabel=r"$P_k$ ratio",
                lpicola_z=lpicola_z))
        

    """
    shutil.rmtree(halos_tmpdir)
    """
    fig.savefig("./pss_mod_unmod.pdf")

    """
    numpy.savetxt(ratios_path, ratios)

    for paths, out_path_gif, do in zip(
        (paths_img, paths_pdf, paths_hmf, paths_ps, paths_psr),
        (
            out_path_img_gif, out_path_pdf_gif, out_path_hmf_gif,
            out_path_ps_gif, out_path_psr_gif),
        (do_img, do_pdf, do_hmf, do_ps, do_psr)):
        if do:
            os.system(" ".join([
                "convert",# "-dispose", "Background", "-background", "black",
                "-delay", str(dt), "-loop", "0"] + paths + [out_path_gif]))
    """
