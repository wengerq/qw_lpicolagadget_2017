# ##### BEGIN GPL LICENSE BLOCK #####
#
#    compare_all_6Gpc_100Mpc.py - Draw stat. quantities for 6Gpc & 100Mpc boxes
#    Copyright (C) 2017  Quentin Wenger
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####


import os
import sys
import re
import math
import itertools
import collections
import tempfile
import shutil

import matplotlib.pyplot as plt
import numpy

sys.path.append("../..")
from qw_analyse_gadget.utils.pynbody_custom import pynbody

from qw_analyse_gadget.analysis import power_spectrum
from qw_analyse_gadget.analysis import probability_density_function
from qw_analyse_gadget.analysis import halo_mass_function
from qw_analyse_gadget.analysis import manual_density

from qw_analyse_gadget.utils import smb_snapshot_reader
from qw_analyse_gadget.utils import smb_downloader



snapshot_gadget_big = "/refreg/data/L-PICOLA/Raphael/Box_6Gpc_gadget/snapshot_150"
snapshot_lpicola_big = [
    "/refreg/data/L-PICOLA/Raphael/Box_6Gpc_snapshot/out_z0p000.%s" % i
    for i in range(64)]
snapshot_gadget_small = "/refreg/data/L-PICOLA/Quentin/New_Sim_3D_20160615_095936_128/snapshot_051"
snapshot_lpicola_small = [
    "/refreg/data/L-PICOLA/Quentin/l-picola_w_gadget_params/Nmesh_512/"\
    "Box_100Mpc_o_h/Npart_128/out_z0p000.%s" % i
    for i in range(64)]




eps = 0.01

out_file = "compare_all_6Gpc_100Mpc.pdf"




def getPowerSpectrum_Ref(z_value):
    base_path = "/some/path/downloads/L-PICOLA/"\
        "Quentin/theory_w_gadget_params"
    fname = "pk_nonlin_z%s.npy" % ("%.3f" % z_value).replace(".", "p")

    npy_file = os.path.join(base_path, fname)

    # XXXX: temp
    data = numpy.load(npy_file)
    return (data[:, 0], data[:, 1])


def getFileNameBase(files):
    # we can assume all files to be in the same dir
    dir_path = os.path.dirname(files[0])
    # inspired by http://stackoverflow.com/questions/18715688/find-common-substring-between-two-strings
    def _iter():
        for chars in zip(*[os.path.basename(f) for f in files]):
            if len(set(chars)) == 1:
                yield chars[0]
            else:
                return


    common_name = ''.join(_iter())
    if not common_name:
        # XXX: anything better???
        common_name = "dummy"

    return os.path.join(dir_path, common_name)



def compute_pdf(dens, bins_width):
    return probability_density_function.pdf_memoryfriendly_bins(
        dens, bins_width, True)

def compute_ps(dens, box_l, bins):
    overdens = power_spectrum.dens2overdens(dens)
    return list(reversed(
        power_spectrum.power_spectrum(overdens, box_l, bin_k=bins)))



def plot_double(ax, title, xlabel, ylabel):
    #fig = plt.figure(figsize=(22, 10))
    main_axis = ax.add_subplot(111)
    main_axis.spines["top"].set_color("none")
    main_axis.spines["bottom"].set_color("none")
    main_axis.spines["left"].set_color("none")
    main_axis.spines["right"].set_color("none")
    main_axis.tick_params(
        labelcolor="w", top="off", bottom="off", left="off", right="off")

    plt.setp(main_axis.get_xticklabels(), visible=False)
    plt.setp(main_axis.get_yticklabels(), visible=False)

    main_axis.set_title(title, fontsize=25, y=1.03)
    main_axis.set_xlabel(xlabel, labelpad=30.0, fontsize=16)
    main_axis.set_ylabel(ylabel, labelpad=35.0, fontsize=16)

    ax1 = ax.add_subplot(1, 2, 1)
    ax2 = ax.add_subplot(1, 2, 2)

    ax.subplots_adjust(wspace=0.1, hspace=0.1)

    return ax1, ax2



def plot_pdf(
    ax,
    pdf, pdf2, label, label2, xmin, xmax, ymin, ymax,
    bins_width
    ):
    ax.bar(
        pdf[1], pdf[0], width=bins_width,
        color=(1.0, 0.0, 0.0, 0.5), label=label)
    ax.bar(
        pdf2[1], pdf2[0], width=bins_width,
        color=(0.0, 0.0, 1.0, 0.5), label=label2)
    ax.set_xlim(xmin, xmax)
    ax.set_ylim(ymin, ymax)
    ax.legend(loc="upper right")
    
    print "PDF"


def plot_hmf(
    ax,
    hmf, hmf2, label, label2, xmin, xmax, ymin, ymax,
    ):
    ax.loglog(hmf[1], hmf[0], label=label)
    ax.loglog(hmf2[1], hmf2[0], label=label2)
    ax.set_xlim(xmin, xmax)
    ax.set_ylim(ymin, ymax)
    ax.legend(loc="upper right")
    print "HMF"

def plot_ps(
    ax,
    ps, ps2, ps_ref, label, label2, label_ref, xmin, xmax, ymin, ymax,
    ):
    ax.loglog(*ps_ref, label=label_ref)
    ax.loglog(*ps, label=label)
    ax.loglog(*ps2, label=label2)
    ax.set_xlim(xmin, xmax)
    ax.set_ylim(ymin, ymax)
    ax.legend(loc="upper right")
    print "PS"
    

def plot_psr(
    ax,
    ps, ps2, ps_ref, label, label2, xmin, xmax, ymin, ymax,
    ):

    ps_ref_range = [[], []]
    for k, _ in zip(*ps):
        i = (numpy.abs(ps_ref[0] - k)).argmin()
        ps_ref_range[0].append(ps_ref[0][i])
        ps_ref_range[1].append(ps_ref[1][i])

    assert (ps[0] == ps2[0]).all()

    ax.plot([xmin, xmax], [1.0, 1.0], "--", color="black")
    ax.plot(ps[0], ps_ref_range[1] / ps2[1], label=label2)
    ax.plot(ps[0], ps[1] / ps2[1], label=label)
    ax.set_xscale("log")
    ax.set_xlim(xmin, xmax)
    ax.set_ylim(ymin, ymax)
    ax.legend(loc="upper right")
    print "PSR"



if __name__ == "__main__":

    halos_tmpdir = tempfile.mkdtemp()

    dens_lpicola_small = "./dens_lpicola_small.npy"
    dens_gadget_small = "./dens_gadget_small.npy"
    dens_lpicola_big = "./dens_lpicola_big.npy"
    dens_gadget_big = "./dens_gadget_big.npy"

    ps_lpicola_small = "./ps_lpicola_small%s.npy"
    ps_gadget_small = "./ps_gadget_small%s.npy"
    ps_lpicola_big = "./ps_lpicola_big%s.npy"
    ps_gadget_big = "./ps_gadget_big%s.npy"

    pdf_lpicola_small = "./pdf_lpicola_small%s.npy"
    pdf_gadget_small = "./pdf_gadget_small%s.npy"
    pdf_lpicola_big = "./pdf_lpicola_big%s.npy"
    pdf_gadget_big = "./pdf_gadget_big%s.npy"

    lpicola_z = 0.0

    fig = plt.figure(figsize=(15, 20))
    ax_ps = fig.add_subplot(4, 1, 1)
    ax_psr = fig.add_subplot(4, 1, 2)
    ax_pdf = fig.add_subplot(4, 1, 3)
    ax_hmf = fig.add_subplot(4, 1, 4)

    ax1_ps = fig.add_subplot(4, 2, 1)
    ax2_ps = fig.add_subplot(4, 2, 2)
    ax1_psr = fig.add_subplot(4, 2, 3)
    ax2_psr = fig.add_subplot(4, 2, 4)
    ax1_pdf = fig.add_subplot(4, 2, 5)
    ax2_pdf = fig.add_subplot(4, 2, 6)
    ax1_hmf = fig.add_subplot(4, 2, 7)
    #ax2_hmf = fig.add_subplot(4, 2, 8)
    ax2_hmf = None

    for ax in (ax_ps, ax_psr, ax_pdf, ax_hmf):
        ax.spines["top"].set_color("none")
        ax.spines["bottom"].set_color("none")
        ax.spines["left"].set_color("none")
        ax.spines["right"].set_color("none")
        ax.tick_params(
            labelcolor="w", top="off", bottom="off", left="off", right="off")

        plt.setp(ax.get_xticklabels(), visible=False)
        plt.setp(ax.get_yticklabels(), visible=False)


    ax_ps.set_title("Power Spectrum", y=1.04)
    ax_psr.set_title("Ratio of Power Spectra", y=1.04)
    ax_pdf.set_title("Probability Distribution Function", y=1.04)
    ax_hmf.set_title("Halo Mass Function", y=1.04)

    ax_ps.set_xlabel(r"$k$ ($h/\mathrm{Mpc}$)", labelpad=20.0)
    ax_ps.set_ylabel(r"$P(k)$ ($(\mathrm{Mpc}/h)^3$)", labelpad=40.0)
    ax_psr.set_xlabel(r"$k$ ($h/\mathrm{Mpc}$)", labelpad=20.0)
    ax_psr.set_ylabel(r"$P(k)$ ratio", labelpad=40.0)
    ax_pdf.set_xlabel("Particles per cell", labelpad=20.0)
    ax_pdf.set_ylabel("Occurrences (normalized)", labelpad=40.0)
    ax_hmf.set_xlabel(r"$M$ ($10^{10} h^{-1} M_{\odot}$)", labelpad=20.0)
    ax_hmf.set_ylabel(r"$\mathrm{d} n / \mathrm{d} \log M$ ($h^3 \mathrm{Mpc}^{-3}$)", labelpad=40.0)

    # TODO: legends
    

    """
        #main_axis.set_title(
        #    "Power Spectrum for $z = %.3f$" % z_value,
        #    y=1.06, fontsize=25)
        # TODO: use usetex, amsmath (\text) and/or siunitx
        main_axis.set_xlabel(
            r"$k$ ($h/\mathrm{Mpc}$)",
            labelpad=30.0, fontsize=16)
        main_axis.set_ylabel(
            r"Power $P(k)$ ($(\mathrm{Mpc}/h)^3$)",
            labelpad=35.0, fontsize=16)
    """

    fig.subplots_adjust(wspace=0.2, hspace=0.4)
        

    for (
        lpicola_files, gadget_file, binning, dens_lpicola, dens_gadget,
        ps_lpicola, ps_gadget, pdf_lpicola, pdf_gadget,
        axis_ps, axis_psr, axis_pdf, axis_hmf,
        pslims, psrlims, pdflims, hmflims,
        sim_small) in zip((
        snapshot_lpicola_small, snapshot_lpicola_big), (
        snapshot_gadget_small, snapshot_gadget_big), (
        1.0, 10.0), (
        dens_lpicola_small, dens_lpicola_big), (
        dens_gadget_small, dens_gadget_big), (
        ps_lpicola_small, ps_lpicola_big), (
        ps_gadget_small, ps_gadget_big), (
        pdf_lpicola_small, pdf_lpicola_big), (
        pdf_gadget_small, pdf_gadget_big), (
        ax1_ps, ax2_ps), (
        ax1_psr, ax2_psr), (
        ax1_pdf, ax2_pdf), (
        ax1_hmf, ax2_hmf), (
        (1e-1, 1e1, 1e1, 1e4), (5e-3, 1e0, 1e2, 1e5)), (
        (1e-1, 1e1, 0.0, 3.0), (5e-3, 1e0, 0.0, 3.0)), (
        (0, 10, 0.0, 0.6), (0, 60, 0.0, 0.1)), (
        (5e1, 1e5, 1e-4, 1e-1), (0, 1, 0, 1)),
        (True, False)):

        if sim_small:

            local_lpicola_files = []
            for lpicola_file in lpicola_files:
                fn = os.path.abspath("./" + os.path.basename(lpicola_file))
                local_lpicola_files.append(fn)
                if not os.path.exists(fn):
                    with open(fn, "wb") as fo:
                        smb_downloader.retrieveFile(lpicola_file, fo)
                    
            local_gadget_file = os.path.abspath("./" + os.path.basename(gadget_file))
            if not os.path.exists(local_gadget_file):
                with open(local_gadget_file, "wb") as fo:
                    smb_downloader.retrieveFile(gadget_file, fo)


            
            sims = []
            for lpicola_file in local_lpicola_files:
                sims.append(pynbody.load(lpicola_file))
                sims[-1].physical_units("Mpc a h^-1", mass="Msol h^-1")
            sim = pynbody.new(sum([len(s) for s in sims]))

            sim_path = os.path.join(
                halos_tmpdir,
                os.path.basename(getFileNameBase(local_lpicola_files)))

            sim.properties = sims[0].properties
            sim["pos"].units = sims[0]["pos"].units
            sim["vel"].units = sims[0]["vel"].units

            sim.write(
                fmt=pynbody.snapshot.gadget.GadgetSnap,
                filename=sim_path,
                format2=False)
            sim = pynbody.load(sim_path)
            sim.physical_units("Mpc a h^-1", mass="Msol h^-1")

            i = 0
            for s in sims:
                le = len(s)
                sim["vel"][i:i + le] = s["vel"]
                sim["pos"][i:i + le] = s["pos"]
                # XXX: integer size problems????
                # XXX: -> try s["iord"].astype(numpy.uint32)
                #if "iord" in s.all_keys():
                #    sim["iord"][i:i + le] = s["iord"]
                i += le

            # if particle indices are not available in L-PICOLA derived snaps
            if (sim["iord"] == 0).all():
                sim["iord"][:] = numpy.arange(len(sim["iord"]))

            # has to save/load twice, in order to have a header bound to the sim,
            # which can them be set to the wanted parameters
            sim.header = sims[0].header
            sim.header.npart = numpy.sum(
                numpy.vstack([s.header.npart for s in sims]), axis=0)
            sim.header.npartTotal = sim.header.npart
            sim.header.num_files = 1

            sim.write(
                fmt=pynbody.snapshot.gadget.GadgetSnap,
                filename=sim_path,
                format2=False)
            sim = pynbody.load(sim_path)
            #sim.physical_units("Mpc a h^-1", mass="Msol h**-1")

            sim2 = pynbody.load(local_gadget_file)
            #sim2.physical_units("Mpc a h^-1", mass="Msol h**-1")

            # XXX: for some reason hmf computing does not work with those,
            # so set them _after_ the hmf calculation...
            #sim.physical_units("Mpc a h**-1", mass="Msol h^-1")
            #sim2.physical_units("Mpc a h**-1", mass="Msol h^-1")
            


            
            # actually the "ignore_ids" is not needed anymore... but we don't need
            # ids either.
            halos = sim.halos(
                eps=eps, filename=sim_path + "_halos", ignore_ids=True)
            halos2 = sim2.halos(
                eps=eps, filename=os.path.join(
                    halos_tmpdir, os.path.basename(local_gadget_file) + "_halos"),
                ignore_ids=True)
        


        """
        a_value = sim.properties["a"]
        lpicola_z = 1.0/a_value - 1.0

        sim.physical_units("Mpc a h**-1", mass="Msol h**-1")
        sim2.physical_units("Mpc a h**-1", mass="Msol h**-1")
        
        pos = sim.dm["pos"]
        pos2 = sim2.dm["pos"]

        box_l = sim2.properties["boxsize"].in_units("Mpc a h**-1")
        h_value = sim2.properties["h"]
        """
        header = smb_snapshot_reader.readSnapshotHeader(gadget_file)
        
        box_l = header.BoxSize
        # well...
        #lpicola_z = header.redshift

        h_value = header.HubbleParam

        #bins = int(round(box_l / 10.0))
        #bins = 128
        bins = int(round(box_l / binning))

        bins_width = 1

        step = 1.1

        max_pos = 10**8

        print "computing dens"

        if os.path.exists(dens_lpicola):
            dens = numpy.load(dens_lpicola)
        else:
            dens = numpy.zeros((bins, bins, bins))
            for lpicola_file in lpicola_files:
                # positions will simply get loaded step-by-step in dens
                manual_density.part2dens3d_smb(dens, lpicola_file, bins, box_l, max_pos=max_pos, keep_alive=False)
            numpy.save(dens_lpicola, dens)
        if os.path.exists(dens_gadget):
            dens2 = numpy.load(dens_gadget)
        else:
            dens2 = numpy.zeros((bins, bins, bins))
            manual_density.part2dens3d_smb(dens2, gadget_file, bins, box_l, max_pos=max_pos, keep_alive=False)
            numpy.save(dens_gadget, dens2)


        print "computing PS"

        if os.path.exists(ps_lpicola % "1"):
            ps = [numpy.load(ps_lpicola % "1"), numpy.load(ps_lpicola % "2")]
        else:
            ps = compute_ps(dens, box_l, bins)
            numpy.save(ps_lpicola % "1", ps[0])
            numpy.save(ps_lpicola % "2", ps[1])
        if os.path.exists(ps_gadget % "1"):
            ps2 = [numpy.load(ps_gadget % "1"), numpy.load(ps_gadget % "2")]
        else:
            ps2 = compute_ps(dens2, box_l, bins)
            numpy.save(ps_gadget % "1", ps2[0])
            numpy.save(ps_gadget % "2", ps2[1])
            
        ps_ref = getPowerSpectrum_Ref(lpicola_z)
        # ref is internally in comoving coords - a Mpc
        # just have to divide by h
        ps_ref = (ps_ref[0]/h_value, ps_ref[1]*h_value**3)

        print "computing PDF"

        if os.path.exists(pdf_lpicola % "1"):
            pdf = (numpy.load(pdf_lpicola % "1"), numpy.load(pdf_lpicola % "2"))
        else:
            pdf = compute_pdf(dens, bins_width)
            numpy.save(pdf_lpicola % "1", pdf[0])
            numpy.save(pdf_lpicola % "2", pdf[1])
        if os.path.exists(pdf_gadget % "1"):
            pdf2 = (numpy.load(pdf_gadget % "1"), numpy.load(pdf_gadget % "2"))
        else:
            pdf2 = compute_pdf(dens2, bins_width)
            numpy.save(pdf_gadget % "1", pdf2[0])
            numpy.save(pdf_gadget % "2", pdf2[1])

        print "plotting pdf"

        plot_pdf(
            ax=axis_pdf,
            pdf=pdf, pdf2=pdf2,
            label="L-PICOLA", label2="GADGET",
            xmin=pdflims[0], xmax=pdflims[1], ymin=pdflims[2], ymax=pdflims[3],
            bins_width=bins_width)

        if sim_small:
            print "plotting hmf"
            
            if len(halos) == 0:
                hmf = ([], [])
            else:
                ms = numpy.stack([h["mass"].sum() for h in halos])
                hmf = halo_mass_function.hmf(ms, step)
            if len(halos2) == 0:
                hmf2 = ([], [])
            else:
                ms = numpy.stack([h["mass"].sum() for h in halos2])
                hmf2 = halo_mass_function.hmf(ms, step)

            plot_hmf(
                ax=axis_hmf,
                hmf=hmf, hmf2=hmf2,
                label="L-PICOLA" + ("" if len(halos) else " (void)"),
                label2="GADGET" + ("" if len(halos2) else " (void)"),
                xmin=hmflims[0], xmax=hmflims[1], ymin=hmflims[2], ymax=hmflims[3])


        print "plotting PS & PSR"

        plot_ps(
            ax=axis_ps,
            ps=ps, ps2=ps2, ps_ref=ps_ref,
            label="L-PICOLA", label2="GADGET", label_ref="Theory",
            xmin=pslims[0], xmax=pslims[1], ymin=pslims[2], ymax=pslims[3])

        plot_psr(
            ax=axis_psr,
            ps=ps, ps2=ps2, ps_ref=ps_ref,
            label="L-PICOLA / GADGET", label2="Theory / GADGET",
            xmin=psrlims[0], xmax=psrlims[1], ymin=psrlims[2], ymax=psrlims[3])


    fig.savefig(out_file)

    shutil.rmtree(halos_tmpdir)

