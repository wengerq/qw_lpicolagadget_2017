# ##### BEGIN GPL LICENSE BLOCK #####
#
#    power_spectra_all_6Gpc_9Gpc_directps.py - Draw PS of big L-PICOLA
#    snapshots via direct density computation
#    Copyright (C) 2017  Quentin Wenger
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####


import os
import sys
import re
import math
import itertools
import collections

import pynbody
import matplotlib.pyplot as plt
import numpy


sys.path.append("../..")
from qw_analyse_gadget.analysis import power_spectrum
from qw_analyse_gadget.analysis import manual_density
from qw_analyse_gadget.utils import smb_downloader
from qw_analyse_gadget.utils import files_manager



Z_VALUES = [
    0.000,
    #0.100,
    #0.200,
    #0.300,
    #0.400,
    #0.500,
    ]

BOXES = collections.OrderedDict([
    #("50Mpc", [256]),
    #("100Mpc", [128, 256, 512]),
    #("1Gpc", [256]),
    #("4Gpc", [256]),
    ("6Gpc", [1024]),
    ("9Gpc", [1024]),
    ])


CACHE_PATH = "/some/path/downloads"
OUTPUTS_PATH = "/some/path/outputs"

BASE_DATA_DIR = "/refreg/data"


# TODO: makedirs, etc...
global_numpy_save = lambda filename, data: numpy.save(
    os.path.normpath(os.path.join(os.getcwd(), filename)), data,
    allow_pickle=False)


def cacheSMBFile(dir_path, filename):
    local_path = os.path.join(CACHE_PATH, dir_path, filename)
    print "Looking at SMB file..."
    print "     dir path: " + dir_path
    print "    file name: " + filename
    if os.path.exists(local_path):
        print "    using cached file in folder %s." % CACHE_PATH
    else:
        with files_manager.LocalFile(local_path) as (fd, _):
            print "    downloading to folder %s..." % CACHE_PATH
            smb_downloader.retrieveFile(
                os.path.join(BASE_DATA_DIR, dir_path, filename), fd)
            fd.close()
            print "    done.\n"
    return CACHE_PATH, os.path.join(dir_path, filename)


def listSMBDir_Dirs(dir_path):
    return smb_downloader.getDirsList(os.path.join(BASE_DATA_DIR, dir_path))

def listSMBDir_Files(dir_path):
    return smb_downloader.getFilesList(os.path.join(BASE_DATA_DIR, dir_path))



def getDataFiles_Gadget(z_value, size, bins):
    base_path = "cpod"

    if size == "100Mpc":
        sim_pattern = re.compile(
            r"^Sim_3D_[0-9]{8}_[0-9]{6}_%s$" % bins)
    else:
        sim_pattern = re.compile(
            r"^Sim_3D_[0-9]{8}_[0-9]{6}_%s_%s$" % (bins, size))

    # just pick up the first corresponding one...
    folder = os.path.join(base_path, [
        d for d in listSMBDir_Dirs(base_path)
        if re.match(sim_pattern, d)][0])

    with open(
        os.path.join(*cacheSMBFile(folder, "outfile_a.dat")), "r") as a_data:
        a_values = [float(line.strip("\n")) for line in a_data.readlines()]

    a_value = 1.0/(1.0 + z_value)
    nearest_a = sorted(a_values, key=lambda a: abs(a - a_value))[0]
    nearest_z = 1.0/nearest_a - 1.0

    # XXX: what about snapshot_000???
    return [cacheSMBFile(
        os.path.join(folder, "3D_boxes"),
        "snapshot_%03i" % (a_values.index(nearest_a) + 1))], nearest_z




def getDataFiles_LPicola(z_value, size, bins):
    base_path = "L-PICOLA/Quentin/l-picola_w_gadget_params/Box_%s_o_h" % size

    pattern = re.compile(r"^out_z%s\.[0-9]+$" % (
        "%.3f" % z_value).replace(".", "p"))

    if len(BOXES[size]) > 1:
        base_path = os.path.join(base_path, "Npart_%s" % bins)
    # else we assume the bins to correspond!
    
    files = listSMBDir_Files(base_path)

    return [
        cacheSMBFile(base_path, f) for f in files
        if re.match(pattern, f)], z_value


### TODO: share code
def getDataFiles_LPicola_no_h(z_value, size, bins):
    base_path = "L-PICOLA/Quentin/l-picola_w_gadget_params/Box_%s" % size

    pattern = re.compile(r"^out_z%s\.[0-9]+$" % (
        "%.3f" % z_value).replace(".", "p"))

    if len(BOXES[size]) > 1:
        base_path = os.path.join(base_path, "Npart_%s" % bins)
    # else we assume the bins to correspond!
    
    files = listSMBDir_Files(base_path)

    return [
        cacheSMBFile(base_path, f) for f in files
        if re.match(pattern, f)], z_value


### TODO: share code
def getDataFiles_LPicola_nmesh512(z_value, size, bins):
    base_path = "L-PICOLA/Quentin/l-picola_w_gadget_params/Nmesh_512/Box_%s_o_h" % size

    pattern = re.compile(r"^out_z%s\.[0-9]+$" % (
        "%.3f" % z_value).replace(".", "p"))

    if len(BOXES[size]) > 1:
        base_path = os.path.join(base_path, "Npart_%s" % bins)
    # else we assume the bins to correspond!
    
    files = listSMBDir_Files(base_path)

    return [
        cacheSMBFile(base_path, f) for f in files
        if re.match(pattern, f)], z_value


### TODO: share code
def getDataFiles_LPicola_nmesh1024(z_value, size, bins):
    base_path = "L-PICOLA/Quentin/l-picola_w_gadget_params/Nmesh_1024/Box_%s_o_h" % size

    pattern = re.compile(r"^out_z%s\.[0-9]+$" % (
        "%.3f" % z_value).replace(".", "p"))

    if len(BOXES[size]) > 1:
        base_path = os.path.join(base_path, "Npart_%s" % bins)
    # else we assume the bins to correspond!
    
    files = listSMBDir_Files(base_path)

    return [
        cacheSMBFile(base_path, f) for f in files
        if re.match(pattern, f)], z_value




def getFileNameBase(files):
    # we can assume all files to be in the same dir
    dir_path = os.path.dirname(files[0])
    # inspired by http://stackoverflow.com/questions/18715688/find-common-substring-between-two-strings
    def _iter():
        for chars in zip(*[os.path.basename(f) for f in files]):
            if len(set(chars)) == 1:
                yield chars[0]
            else:
                return


    common_name = ''.join(_iter())
    if not common_name:
        # XXX: anything better???
        common_name = "dummy"

    return os.path.join(dir_path, common_name)




def distributedDensity(files, z_value, bins, mul_h, separate):

    print "calculating distributed density"

    dens = numpy.zeros((bins, bins, bins))

    
    box_l = None
    ### TODO: what about separate? make it simply bool or use it?

    # proper to comoving
    a_value = 1.0/(1.0 + z_value)
    

    for i, f_comps in enumerate(files):
        sim = pynbody.load(os.path.join(*f_comps))
        sim.physical_units("Mpc")
        if box_l is None:
            # should be the same for all files, even if split simulation
            # we could also derive this from function param size...
            box_l = sim.properties["boxsize"].in_units(
                "Mpc")
            if mul_h:
                box_l *= sim.properties["h"]
            # also convert box_l??
            box_l /= a_value
        p = sim.dm["pos"]
        del sim
        """
        dens += power_spectrum.part2dens3d(p/a_value, box_l, bins)
        """
        hist, _edges = numpy.histogramdd(
            p/a_value, bins=bins, range=[[0, box_l], [0, box_l], [0, box_l]])
        del _edges
        del p
        dens += hist
        del hist
        print i


    print len(dens), dens.nbytes

    return dens



def getPowerSpectrum_Sim(fct, z_value, size, bins, mul_h=True, separate=0):

    files, nearest_z = fct(z_value, size, bins)

    if not files:
        return ([], []), z_value    

    base = os.path.join(
        OUTPUTS_PATH, getFileNameBase([f[1] for f in files]))
    power_base = base + "_ps"
    power_k = power_base + "_k"
    power_p = power_base + "_p"
    density_path = base + "_d_%s.npy" % density_bins(size)

    if os.path.exists(power_p) and os.path.exists(power_k):
        print "Looking at output files..."
        print "    " + power_k
        print "    " + power_p
        print "    using cached files."
        ps = (
            numpy.loadtxt(power_k),
            numpy.loadtxt(power_p))

    else:

        # TODO: share code?
        if separate:

            box_l = None

            for f_comps in files:
                sim = pynbody.load(os.path.join(*f_comps))
                sim.physical_units("Mpc")
                if box_l is None:
                    # should be the same for all files, even if split simulation
                    # we could also derive this from function param size...
                    box_l = sim.properties["boxsize"].in_units(
                        "Mpc")
                    if mul_h:
                        box_l *= sim.properties["h"]
                    del sim
                    break

            print "Looking at output files..."
            print "    " + power_k
            print "    " + power_p
            print "    caching..."

            if os.path.exists(density_path):
                dens = numpy.load(density_path)
            else:
                print "        caching density..."
                dens = distributedDensity(files, z_value, bins, mul_h, separate)
                global_numpy_save(density_path, dens)
            #overdens = power_spectrum.dens2overdens(dens)
            #del dens
            overdens = dens
            manual_density.dens2overdens_inplace(overdens)
            del dens # just the variable, not the data (still ref'ed by overdens)
            
            #ps = list(reversed(
            #    power_spectrum.power_spectrum(overdens, box_l, bin_k=bins)))



            # XXX: just a copy of power_spectrum.power_spectrum, reordered
            # and with deleting as soon as possible to free space.

            field_x = overdens
            del overdens # just the variable, not the data (still ref'ed by field_x)
            bin_k = bins
            
            assert numpy.ndim(field_x) == 3, 'field_x is not 3D'
            box_pix = numpy.size(field_x, axis=0)  # pixel number per axis
            box_dim = numpy.ndim(field_x)  # dimension

            print 1
            _fs = numpy.fft.fftshift(field_x)
            print 2
            del field_x # here the data should really get deleted.
            print 3
            _field_k = numpy.fft.rfftn(_fs) * (box_l/box_pix)**box_dim
            print 4
            del _fs
            print 5


            # This 'paragraph' is to create masks of indices corresponding to one Fourier bin each
            _freq = numpy.fft.fftfreq(n=box_pix, d=box_l/box_pix) * 2*numpy.pi
            _rfreq = numpy.fft.rfftfreq(n=box_pix, d=box_l/box_pix) * 2*numpy.pi
            _kx, _ky, _kz = numpy.meshgrid(_freq, _freq, _rfreq, indexing='ij')
            del _freq, _rfreq
            _k_abs = numpy.sqrt(_kx**2. + _ky**2. + _kz**2.)
            del _kx, _ky, _kz
            print 6
            # The following complicated line is actually only creating a 1D array spanning k-space logarithmically from minimum _k_abs to maximum.
            # To start slightly below the minimum and finish slightly above the maximum I use ceil and floor.
            # To ceil and floor not to the next integer but to the next 15th digit, I multiply by 1e15 before flooring and divide afterwards.
            # Since the ceiled/floored value is actually the exponent used for the logspace, going to the next integer would be way too much.
            _k_log = numpy.logspace(numpy.floor(numpy.log10(numpy.min(_k_abs[1:]))*1.e15)/1.e15, numpy.ceil(numpy.log10(numpy.max(_k_abs[1:]))*1.e15)/1.e15, bin_k)
            print 7
            power_k_array = numpy.empty(numpy.size(_k_log)-1)
            print 8
            for i in xrange(numpy.size(_k_log)-1):
                mask = (_k_abs >= _k_log[i]) & (_k_abs < _k_log[i+1])
                power_k_array[i] = numpy.mean(numpy.abs(_field_k[mask])**2.) / box_l**box_dim
            del _k_abs, _field_k
            print 9

            k = (_k_log[1:] + _k_log[:-1]) / 2
            del _k_log
            print 10
            ps = [k, power_k_array]

            # XXX: end

            

            basedir = os.path.dirname(power_base)
            if not os.path.exists(basedir):
                os.makedirs(basedir)
            numpy.savetxt(power_k, ps[0])
            numpy.savetxt(power_p, ps[1])

            print "    done.\n"
            

        else:

            positions = []

            box_l = None

            for f_comps in files:
                sim = pynbody.load(os.path.join(*f_comps))
                sim.physical_units("Mpc")
                if box_l is None:
                    # should be the same for all files, even if split simulation
                    # we could also derive this from function param size...
                    box_l = sim.properties["boxsize"].in_units(
                        "Mpc")
                    if mul_h:
                        box_l *= sim.properties["h"]
                positions.append(sim.dm["pos"])

            np_positions = numpy.vstack(positions)


            # proper to comoving
            a_value = 1.0/(1.0 + z_value)
            np_positions /= a_value
            # also convert box_l??
            box_l /= a_value



            print "Looking at output files..."
            print "    " + power_k
            print "    " + power_p
            print "    caching..."

            if os.path.exists(density_path):
                dens = numpy.load(density_path)
            else:
                print "        caching density..."
                dens = power_spectrum.part2dens3d(np_positions, box_l, bins)
                global_numpy_save(density_path, dens)
            #overdens = power_spectrum.dens2overdens(dens)
            #del dens
            overdens = dens
            manual_density.dens2overdens_inplace(overdens)
            ps = list(reversed(
                power_spectrum.power_spectrum(overdens, box_l, bin_k=bins)))

            basedir = os.path.dirname(power_base)
            if not os.path.exists(basedir):
                os.makedirs(basedir)
            numpy.savetxt(power_k, ps[0])
            numpy.savetxt(power_p, ps[1])

            print "    done.\n"


    return ps, nearest_z



def getPowerSpectrum_Gadget(z_value, size, bins):
    return getPowerSpectrum_Sim(getDataFiles_Gadget, z_value, size, bins)
def getPowerSpectrum_LPicola(z_value, size, bins):
    return getPowerSpectrum_Sim(getDataFiles_LPicola, z_value, size, bins)
def getPowerSpectrum_LPicola_no_h(z_value, size, bins, separate=0):
    return getPowerSpectrum_Sim(getDataFiles_LPicola_no_h, z_value, size, bins, mul_h=False, separate=separate)
def getPowerSpectrum_LPicola_nmesh512(z_value, size, bins):
    return getPowerSpectrum_Sim(getDataFiles_LPicola_nmesh512, z_value, size, bins)
def getPowerSpectrum_LPicola_nmesh1024(z_value, size, bins):
    return getPowerSpectrum_Sim(getDataFiles_LPicola_nmesh1024, z_value, size, bins)

def getPowerSpectrum_Ref(z_value, z_prec=3):
    base_path = "L-PICOLA/Quentin/theory_w_gadget_params"
    fname = "pk_nonlin_z%s.npy" % (("%." + str(z_prec) + "f") % z_value).replace(".", "p")

    npy_file = os.path.join(*cacheSMBFile(base_path, fname))

    data = numpy.load(npy_file)
    return (data[:, 0], data[:, 1]), z_value




density_bins = lambda size: {
    "50Mpc": 5,
    "100Mpc": 10,
    "1Gpc": 100,
    "4Gpc": 400,
    "6Gpc": 600,
    "9Gpc": 900}[size]



def makePartialDensities():
    print "making partial densities..."
    for z_value in Z_VALUES:
        print z_value

        items = BOXES.items()
        total_items = sum(len(x[1]) for x in items)


        y_amount = int(math.ceil(total_items**0.5))
        x_amount = int(math.ceil(float(total_items)/y_amount))
        i = 1

        for size, bins_list in items:
            for bins in bins_list:
                print size, bins
                i += 1


                files, nearest_z = getDataFiles_LPicola_no_h(
                    z_value, size, density_bins(size))

                for f in files:
                    dens_path = os.path.join(
                        OUTPUTS_PATH, getFileNameBase([f[1]]) + "_d_%s.npy" % density_bins(size))

                    if os.path.exists(dens_path):
                        print "using density from", dens_path
                    else:
                        dens = distributedDensity(
                            [f], z_value, density_bins(size),
                            mul_h=False, separate=density_bins(size))

                        print "saving density..."

                        global_numpy_save(dens_path, dens)

                        print "saved density", dens_path
    


def makeTotalDensity():
    print "making total density..."
    for z_value in Z_VALUES:
        print z_value

        items = BOXES.items()
        total_items = sum(len(x[1]) for x in items)


        y_amount = int(math.ceil(total_items**0.5))
        x_amount = int(math.ceil(float(total_items)/y_amount))
        i = 1

        for size, bins_list in items:
            for bins in bins_list:
                print size, bins
                i += 1

                files, nearest_z = getDataFiles_LPicola_no_h(
                    z_value, size, density_bins(size))

                tot_dens_path = os.path.join(
                    OUTPUTS_PATH, getFileNameBase([f[1] for f in files]) + "_d_%s.npy" % density_bins(size))

                if os.path.exists(tot_dens_path):
                    print "using density from", tot_dens_path

                else:

                    tot_dens = numpy.zeros((
                        density_bins(size), density_bins(size), density_bins(size)))

                    for f in files:
                        print f
                        dens_path = os.path.join(
                            OUTPUTS_PATH, getFileNameBase([f[1]]) + "_d_%s.npy" % density_bins(size))

                        tot_dens += numpy.load(dens_path)

                    global_numpy_save(tot_dens_path, tot_dens)
                    print "saved total density."
                


def makeTotalDensity_direct():
    print "making total density directly..."
    for z_value in Z_VALUES:
        print z_value

        items = BOXES.items()
        total_items = sum(len(x[1]) for x in items)


        y_amount = int(math.ceil(total_items**0.5))
        x_amount = int(math.ceil(float(total_items)/y_amount))
        i = 1

        for size, bins_list in items:
            for bins in bins_list:
                print size, bins
                i += 1

                files, nearest_z = getDataFiles_LPicola_no_h(
                    z_value, size, density_bins(size))

                tot_dens_path = os.path.join(
                    OUTPUTS_PATH, getFileNameBase([f[1] for f in files]) + "_d_%s.npy" % density_bins(size))

                if os.path.exists(tot_dens_path):
                    print "using density from", tot_dens_path

                else:

                    tot_dens = numpy.zeros((
                        density_bins(size), density_bins(size), density_bins(size)))

                    for f in files:
                        print f
                        tot_dens += distributedDensity(
                            [f], z_value, density_bins(size),
                            mul_h=False, separate=density_bins(size))

                    global_numpy_save(tot_dens_path, tot_dens)
                    print "saved total density."




def makeTotalDensity_manual():
    print "making total density manually..."
    for z_value in Z_VALUES:
        print z_value

        items = BOXES.items()
        total_items = sum(len(x[1]) for x in items)


        y_amount = int(math.ceil(total_items**0.5))
        x_amount = int(math.ceil(float(total_items)/y_amount))
        i = 1

        for size, bins_list in items:
            for bins in bins_list:
                print size, bins
                i += 1

                files, nearest_z = getDataFiles_LPicola_no_h(
                    z_value, size, density_bins(size))

                tot_dens_path = os.path.join(
                    OUTPUTS_PATH, getFileNameBase([f[1] for f in files]) + "_d_%s.npy" % density_bins(size))

                if os.path.exists(tot_dens_path):
                    print "using density from", tot_dens_path

                else:

                    tot_dens = numpy.zeros((
                        density_bins(size), density_bins(size), density_bins(size)))

                    for f in files:
                        print f
                        manual_density.part2dens3d(tot_dens, f, z_value, density_bins(size), mul_h=False)

                    global_numpy_save(tot_dens_path, tot_dens)
                    print "saved total density."




def main():
    #makePartialDensities()
    #makeTotalDensity()
    #makeTotalDensity_direct()
    makeTotalDensity_manual()
    for z_value in Z_VALUES:

        fig = plt.figure(figsize=(15, 15))

        items = BOXES.items()
        total_items = sum(len(x[1]) for x in items)

        main_axis = fig.add_subplot(111)
        #http://stackoverflow.com/questions/6963035/pyplot-axes-labels-for-subplots
        main_axis.spines["top"].set_color("none")
        main_axis.spines["bottom"].set_color("none")
        main_axis.spines["left"].set_color("none")
        main_axis.spines["right"].set_color("none")
        main_axis.tick_params(
            labelcolor="w", top="off", bottom="off", left="off", right="off")

        plt.setp(main_axis.get_xticklabels(), visible=False)
        plt.setp(main_axis.get_yticklabels(), visible=False)


        #main_axis.set_title(
        #    "Power Spectrum for $z = %.3f$ (smoothing: 10Mpc)" % z_value,
        #    y=1.06, fontsize=25)
        # TODO: use usetex, amsmath (\text) and/or siunitx
        main_axis.set_xlabel(
            r"$k$ ($h/\mathrm{Mpc}$)",
            labelpad=30.0, fontsize=16)
        main_axis.set_ylabel(
            r"Power $P(k)$ ($(\mathrm{Mpc}/h)^3$)",
            labelpad=35.0, fontsize=16)

        fig.subplots_adjust(wspace=0.1, hspace=0.2)

        y_amount = int(math.ceil(total_items**0.5))
        x_amount = int(math.ceil(float(total_items)/y_amount))
        i = 1

        for size, bins_list in items:
            for bins in bins_list:
                # TODO: shares
                ax = fig.add_subplot(y_amount, x_amount, i)
                i += 1

                """
                ps_gadget, z_gadget = getPowerSpectrum_Gadget(
                    z_value, size, bins)
                ps_lpicola, z_lpicola = getPowerSpectrum_LPicola(
                    z_value, size, bins)
                ps_lpicola_512, z_lpicola_512 = getPowerSpectrum_LPicola_nmesh512(
                    z_value, size, bins)
                ps_lpicola_1024, z_lpicola_1024 = getPowerSpectrum_LPicola_nmesh1024(
                    z_value, size, bins)
                """
                ps_lpicola, z_lpicola = getPowerSpectrum_LPicola_no_h(
                    z_value, size, density_bins(size), separate=density_bins(size))

                """
                if len(ps_gadget[0]) and len(ps_lpicola[0]):
                    # XXX: why do we need *2 here???
                    min_k = min(ps_gadget[0][0], ps_lpicola[0][0], ps_lpicola_512[0][0], ps_lpicola_1024[0][0])*2
                    max_k = max(ps_gadget[0][-1], ps_lpicola[0][-1], ps_lpicola_512[0][-1], ps_lpicola_1024[0][-1])*2
                    min_p = min(
                        min(v for v in ps_gadget[1] if not numpy.isnan(v)),
                        min(v for v in ps_lpicola[1] if not numpy.isnan(v)),
                        min(v for v in ps_lpicola_512[1] if not numpy.isnan(v)),
                        min(v for v in ps_lpicola_1024[1] if not numpy.isnan(v)))/2
                    max_p = max(
                        max(v for v in ps_gadget[1] if not numpy.isnan(v)),
                        max(v for v in ps_lpicola[1] if not numpy.isnan(v)),
                        max(v for v in ps_lpicola_512[1] if not numpy.isnan(v)),
                        max(v for v in ps_lpicola_1024[1] if not numpy.isnan(v)))*2

                elif len(ps_gadget[0]):
                    # TODO: ps_gadget_512?
                    min_k = min(ps_gadget[0][0]*2, ps_gadget_512[0][0]*2, ps_gadget_1024[0][0]*2)
                    max_k = max(ps_gadget[0][-1]*2, ps_gadget_512[0][-1]*2, ps_gadget_1024[0][-1]*2)
                    min_p = min(v for v in ps_gadget[1] if not numpy.isnan(v))/2
                    max_p = max(v for v in ps_gadget[1] if not numpy.isnan(v))*2

                elif len(ps_lpicola[0]):
                """
                if len(ps_lpicola[0]):
                    min_k = ps_lpicola[0][0]*2
                    max_k = ps_lpicola[0][-1]*2
                    min_p = min(v for v in ps_lpicola[1] if not numpy.isnan(v))/2
                    max_p = max(v for v in ps_lpicola[1] if not numpy.isnan(v))*2

                # XXX: any nicer way?
                
                ps_ref, z_ref = getPowerSpectrum_Ref(z_value, z_prec=1)

                if len(ps_ref[0]):
                    plt.loglog(
                        *ps_ref,
                        label=r"$P_\mathrm{Theory}$, $z = %.3f$" % z_ref)
                """
                if len(ps_gadget[0]):
                    plt.loglog(
                        *ps_gadget,
                        label=r"$P_\mathrm{Gadget}$, $z = %.3f$" % z_gadget)
                if len(ps_lpicola[0]):
                    plt.loglog(
                        *ps_lpicola,
                        label=r"$P_\mathrm{PICOLA, old}$, $z = %.3f$" % z_lpicola)
                if len(ps_lpicola_512[0]):
                    plt.loglog(
                        *ps_lpicola_512,
                        label=r"$P_\mathrm{PICOLA, Nmesh 512}$, $z = %.3f$" % z_lpicola_512)
                if len(ps_lpicola_1024[0]):
                    plt.loglog(
                        *ps_lpicola_1024,
                        label=r"$P_\mathrm{PICOLA, Nmesh 1024}$, $z = %.3f$" % z_lpicola_1024)
                """
                if len(ps_lpicola[0]):
                    plt.loglog(
                        *ps_lpicola,
                        label=r"$P_\mathrm{PICOLA, Nmesh 2048}$, $z = %.3f$" % z_lpicola)
                
                ax.set_title(r"%s, $N_\mathrm{part} = %s$" % (size, bins))
                ax.set_xlim(min_k, max_k)
                ax.set_ylim(min_p, max_p)
                ax.legend(loc="lower left")


        plt.savefig("./power_spectra_all_6Gpc_9Gpc_%s.pdf" % (
            ("%.3f" % z_value).replace(".", "p")))
        #plt.show()
        print "-----------------"



if __name__ == "__main__":
    main()
