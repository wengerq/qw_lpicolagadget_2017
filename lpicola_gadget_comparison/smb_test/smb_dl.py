# ##### BEGIN GPL LICENSE BLOCK #####
#
#    smb_dl.py - Testing SMB server interaction
#    Copyright (C) 2017  Quentin Wenger
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####


import getpass

from smb.SMBConnection import SMBConnection



def getFilesList(path="refreg/data/cpod"):
    username = "wengerq"
    password = getpass.getpass("Password for %s: " % username)
    c = SMBConnection(username, password, "wengerqpy", "astrogate", "AD")
    c.connect("astrogate")
    fs = [x.filename for x in c.listPath("astro", path) if not x.isDirectory]
    c.close()
    return fs


def main():
    print
    print "\n".join(getFilesList("refreg/data/cpod/Sim_3D_20140424_172232_A"))

if __name__ == "__main__":
    main()
