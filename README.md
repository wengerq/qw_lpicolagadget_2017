Public repository for files of the "Comparison of L-PICOLA and GADGET simulations" semester project.

Note: some of the files contain placeholder paths (like "/some/path/") that are meant to be replaced by something meaningful by the users.
In particular, the file ./qw_analyse_gadget/config.ini may (should) be changed in order to give access to a customized pynbody package, if needed.