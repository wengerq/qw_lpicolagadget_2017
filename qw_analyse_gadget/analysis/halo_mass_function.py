# ##### BEGIN GPL LICENSE BLOCK #####
#
#    halo_mass_function.py - Implement HMF computation from halo masses
#    Copyright (C) 2017  Quentin Wenger
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####


import numpy as np


def hmf(masses, step):
    a = np.log(masses.min())
    b = np.log(masses.max())
    values = np.exp(np.arange(a, b + np.log(step), np.log(step)))
    counts = np.zeros(values.shape)
    for v in np.nditer(masses):
        counts[int((np.log(v) - a)/np.log(step))] += 1
    counts /= counts.sum()


    return counts, values
