# ##### BEGIN GPL LICENSE BLOCK #####
#
#    probability_density_function.py - Compute PDF
#    Copyright (C) 2017  Quentin Wenger
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####


import numpy as np


def pdf(density, bins="auto", r=1.0):
    # auto flattening??
    f = density.flatten()
    #print float(np.count_nonzero(f))/len(f)
    #print float(sum((1 if i == 1 else 0) for i in f))/len(f)
    a = int(f.min())
    b = int(f.max())
    #h, e = np.histogram(f, bins)
    #h, e = np.histogram(f, density=True, range=(a, b), bins=b - a + 1)
    h, e = np.histogram(f, bins=bins, range=(a, b/r))
    #h, e = np.histogram(f, density=True, bins="auto", range=(a, b/200))
    #print h
    #print e
    return h, e

def pdf_memoryfriendly(density, bins="auto", r=1.0):
    # TODO use bins/r...
    a = int(density.min())
    b = int(density.max())
    values = np.arange(a, b + 1)
    counts = np.zeros(values.shape, dtype="int")
    for v in np.nditer(density):
        counts[int(v) - a] += 1

    return counts, values


def pdf_memoryfriendly_bins(density, step, norm=False):
    b = int(density.max())
    values = np.arange(0, b + step, step)
    if norm:
        counts = np.zeros(values.shape)
    else:
        counts = np.zeros(values.shape, dtype="int")
    for v in np.nditer(density):
        counts[int(v/step)] += 1
    if norm:
        counts /= counts.sum()


    return counts, values
