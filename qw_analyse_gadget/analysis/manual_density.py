# ##### BEGIN GPL LICENSE BLOCK #####
#
#    manual_density.py - Implement density field computation using loops over
#    position data minimizing memory usage
#    Copyright (C) 2017  Quentin Wenger
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####


import numpy as np
import os
import sys


from ..utils.pynbody_custom import pynbody
from ..utils import smb_snapshot_reader


def part2dens3d(density_array, f, z_value, bins, mul_h=False):

    # proper to comoving
    a_value = 1.0/(1.0 + z_value)
    

    sim = pynbody.load(os.path.join(*f))
    sim.physical_units("Mpc")

    box_l = sim.properties["boxsize"].in_units("Mpc")
    if mul_h:
        box_l *= sim.properties["h"]
        # also convert box_l??
    box_l /= a_value

    # is this -1 physically correct???
    bin_size = box_l/(bins - 1)

    p = sim.dm["pos"]/a_value
    del sim

    # just mimic numpy.histogramdd...
    # probably quite slow but memory efficient...
    # TODO: make it anyways a bit more efficient with numpy arrays?...
    for i, point in enumerate(p):
        if not i % 10000000:
            print i
        """
        for coord in point:
            # aka range
            # XXX: < box_l -> b/c of int(box_l/bin_size) == bins, so out of bounds
            # ... does histogramdd work the same way?
            if not (0 <= coord < box_l):
                break
        else:
        """
        density_array[
            int(point[0]/bin_size),
            int(point[1]/bin_size),
            int(point[2]/bin_size)] += 1
    
    del p




def part2dens3d_smb(density_array, f, bins, box_l=None, max_pos=2**10, keep_alive=True):
    # GADGET-style snapshots are already in [a Mpc / h]

    if box_l is None:
        box_l = smb_snapshot_reader.readSnapshotHeader(f).BoxSize

    # is this -1 physically correct???
    bin_size = box_l/(bins - 1)

    it = smb_snapshot_reader.iterSnapshotPositions(
        f, total_max_pos=-1, max_pos=max_pos, keep_alive=keep_alive)

    # just mimic numpy.histogramdd...
    # probably quite slow but memory efficient...
    # TODO: make it anyways a bit more efficient with numpy arrays?...
    for i, point in enumerate(it):
        if not i % 10000000:
            print i
        """
        for coord in point:
            # aka range
            # XXX: < box_l -> b/c of int(box_l/bin_size) == bins, so out of bounds
            # ... does histogramdd work the same way?
            if not (0 <= coord < box_l):
                break
        else:
        """
        density_array[
            int(point[0]/bin_size),
            int(point[1]/bin_size),
            int(point[2]/bin_size)] += 1


    



def dens2overdens_inplace(density, mean_density=None):
    assert np.ndim(density) == 3, 'density is not 3D'
    if mean_density:
        density -= np.mean(density)
        density /= mean_density
    else:
        density /= np.mean(density)
        density -= 1.0


