# ##### BEGIN GPL LICENSE BLOCK #####
#
#    files_manager.py - Local file caching system
#    Copyright (C) 2017  Quentin Wenger
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####


import os
import tempfile

class TempFile(object):
    def __init__(self, suffix="", dir=None):
        self.suffix = suffix
        self.dir = dir
    def __enter__(self):
        # maybe mkstemp would suffice?
        f = tempfile.NamedTemporaryFile(
            suffix=self.suffix, dir=self.dir, delete=False)
        self.path = f.name
        self.f = f
        return self.f
    def __exit__(self, exc_type, exc_value, traceback):
        if not self.f.close_called:
            self.f.close()
        os.remove(self.path)


class LocalFile(object):
    def __init__(self, path):
        self.path = path

    def __enter__(self):
        exists = os.path.exists(self.path)
        if exists:
            self.f = open(self.path, "r+b")
        else:
            if not os.path.exists(os.path.dirname(self.path)):
                os.makedirs(os.path.dirname(self.path))
            self.f = open(self.path, "w+b")
        return (self.f, exists)
    
    def __exit__(self, exc_type, exc_value, traceback):
        if not self.f.closed:
            self.f.close()
