# ##### BEGIN GPL LICENSE BLOCK #####
#
#    pynbody_custom.py - Access to customized pynbody package
#    Copyright (C) 2017  Quentin Wenger
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####


import sys
import os
import ConfigParser

conf = ConfigParser.ConfigParser()
conf.readfp(open(os.path.join(os.path.dirname(__file__), "../config.ini")))

if conf.has_option("pynbody", "path"):
    sys.path.insert(0, conf.get("pynbody", "path"))

# will fallback to normally installed package if the option in conf file
# not successful
import pynbody
