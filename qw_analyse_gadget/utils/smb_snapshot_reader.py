# ##### BEGIN GPL LICENSE BLOCK #####
#
#    smb_snapshot_reader.py - Helper API for reading snapshot data from
#    astrogate server in-RAM
#    Copyright (C) 2017  Quentin Wenger
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####


import getpass
import sys
import io
import struct

from smb.SMBConnection import SMBConnection

import snapshot_reader




creds = {"wengerq": None}
service = "astro"

def createConnection():
    username = "wengerq"
    if creds[username] is None:
        creds[username] = getpass.getpass("Password for %s: " % username)
    c = SMBConnection(username, creds[username], "wengerqpy", "astrogate", "AD")
    c.connect("astrogate")
    return c


def readSnapshotHeader(path, c=None):
    c_was_none = False
    if c is None:
        c_was_none = True
        c = createConnection()

    fo = io.BytesIO()
    c.retrieveFileFromOffset(service, path, fo, 0, 4)
    fo.seek(0)
    assert (
        snapshot_reader.extractSingleElement(fo, "i") ==
        snapshot_reader.HEADER_SIZE)

    fo = io.BytesIO()
    c.retrieveFileFromOffset(
        service, path, fo, struct.calcsize(
            snapshot_reader.ENDIAN + "i"),
        snapshot_reader.HEADER_SIZE)
    fo.seek(0)
    h = snapshot_reader.GADGETHeader.fromFileObject(fo)

    if c_was_none:
        c.close()

    return h


def iterSnapshotPositions(path, c=None, total_max_pos=-1, max_pos=2**10, keep_alive=True):
    c_was_none = False
    if c is None:
        c_was_none = True
        c = createConnection()

    pos0 = (
        struct.calcsize(snapshot_reader.ENDIAN + "i")*2 +
        snapshot_reader.HEADER_SIZE)

    fo = io.BytesIO()
    c.retrieveFileFromOffset(
        service, path, fo, pos0, struct.calcsize(
            snapshot_reader.ENDIAN + "i"))
    fo.seek(0)
    a = snapshot_reader.extractSingleElement(fo, "i")

    bytes_per_pos = 3*struct.calcsize(snapshot_reader.ENDIAN + "f")

    if a == 0:
        # no amount info found, look at header and hope the full amount
        # is there...
        h = readSnapshotHeader(path, c)
        total_amount = h.npart[h.DM]

    else:
        total_amount = int(a/bytes_per_pos)

    if total_max_pos != -1:
        total_amount = min(total_amount, total_max_pos)

    if max_pos == -1:
        max_pos = total_amount

    pos0 += struct.calcsize(snapshot_reader.ENDIAN + "i")


    for delta_amount in range(0, total_amount, max_pos):
        amount0 = delta_amount
        amountn = min(max_pos, total_amount - delta_amount)
        fo = io.BytesIO()
        if not keep_alive:
            c.close()
            c = createConnection()
        c.retrieveFileFromOffset(
            service, path, fo,
            pos0 + bytes_per_pos*amount0, bytes_per_pos*amountn)
        fo.seek(0)
        for _ in range(amountn):
            yield snapshot_reader.extractElements(fo, 3*"f")

    if c_was_none:
        c.close()


def iterMultipleSnapshotPositions(
    paths, c=None, max_pos=2**10):
    # TODO: total_max_pos
    c_was_none = False
    if c is None:
        c_was_none = True
        c = createConnection()

    for path in paths:
        for p in iterSnapshotPositions(path, c, -1, max_pos):
            yield p

    if c_was_none:
        c.close()




def main():
    path = "refreg/data/L-PICOLA/Raphael/Box_6Gpc_gadget/snapshot_066"
    for p in iterSnapshotPositions(path, total_max_pos=200, max_pos=20):
        print p

if __name__ == "__main__":
    main()
