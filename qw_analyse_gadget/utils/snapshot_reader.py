# ##### BEGIN GPL LICENSE BLOCK #####
#
#    snapshot_reader.py - Snapshot parsing utilities
#    Copyright (C) 2017  Quentin Wenger
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####


import struct
import collections
import binascii
import numpy

HEADER_SIZE = 256
ENDIAN = "="


def extractElements(fo, fmt, endian=ENDIAN):
    d = fo.read(struct.calcsize(endian + fmt))
    return struct.unpack(endian + fmt, d)

def extractSingleElement(fo, fmt, endian=ENDIAN):
    return extractElements(fo, fmt, endian)[0]

def extract(fo, fmt, endian=ENDIAN):
    if len(fmt) == 1:
        fct = extractSingleElement
    else:
        fct = extractElements
    return fct(fo, fmt, endian)



class GADGETHeader(object):

    STRUCTURE = [
        ("npart", "IIIIII"),
        ("mass", "dddddd"),
        ("time", "d"),
        ("redshift", "d"),
        ("flag_sfr", "i"),
        ("flag_feedback", "i"),
        ("npartTotal", "IIIIII"),
        ("flag_cooling", "i"),
        ("num_files", "i"),
        ("BoxSize", "d"),
        ("Omega0", "d"),
        ("OmegaLambda", "d"),
        ("HubbleParam", "d"),
        ("flag_stellarage", "i"),
        ("flag_metals", "i"),
        ("npartTotalHighWord", "IIIIII"),
        ("flag_entropy_instead_u", "i"),
        ("fill", "c"*60)]

    GAS = 0
    DM = HALO = 1
    DISK = 2
    BULGE = 3
    STARS = 4
    BOUNDARY = 5

    assert(sum(struct.calcsize(ENDIAN + f) for _, f in STRUCTURE) == HEADER_SIZE)
    
    
    def __init__(self):
        for attr_name, _ in self.STRUCTURE:
            setattr(self, attr_name, None)


    @classmethod
    def fromFileObject(cls, fo):
        obj = cls()
        
        for attr_name, fmt in cls.STRUCTURE:
            setattr(obj, attr_name, extract(fo, fmt))

        return obj

    @property
    def totalDMParts(self):
        return self.npartTotalHighWord[self.DM] << 32 + self.npartTotal[self.DM]





def readHeader(filepath):
    with open(filepath, "rb") as fo:
        d = extractSingleElement(fo, "i")
        assert d == HEADER_SIZE

        return GADGETHeader.fromFileObject(fo)


def readPositions(filepath):
    with open(filepath, "rb") as fo:
        fo.seek(struct.calcsize(ENDIAN + "i")*2 + HEADER_SIZE)
        a = extractSingleElement(fo, "i")
        if a == 0:
            fo.seek(struct.calcsize(ENDIAN + "i"))
            h = GADGETHeader.fromFileObject(fo)
            amount = h.npart[h.DM]
        else:
            bytes_per_pos = 3*struct.calcsize(ENDIAN + "f")
            # py3 compat
            amount = a//bytes_per_pos
        fo.seek(struct.calcsize(ENDIAN + "i")*3 + HEADER_SIZE)
        return numpy.fromfile(fo, numpy.dtype(ENDIAN + "3f"), count=amount)

def readMultiplePositions(filepaths):
    return numpy.vstack([readPositions(f) for f in filepaths])
